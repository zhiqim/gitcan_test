/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.action;

import java.util.ArrayList;
import java.util.List;

import org.zhiqim.gitcan.GitcanConstants;
import org.zhiqim.gitcan.dao.GitCalendarDao;
import org.zhiqim.gitcan.dbo.GitPersonPlan;
import org.zhiqim.gitcan.dbo.GitPersonSummary;
import org.zhiqim.gitcan.model.CalendarWeekModel;
import org.zhiqim.gitcan.model.PersonDayModel;
import org.zhiqim.gitcan.model.PersonModel;
import org.zhiqim.gitcan.model.PersonPlanModel;
import org.zhiqim.gitcan.model.PersonWeekModel;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.kernel.util.DateTimes;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;

/**
 *  个人计划总结月表 
 *
 * @version v1.0.0 @author zouzhigang 2017-11-2 新建与整理
 */
public class MyPlanAction implements Action, GitcanConstants
{
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        String operatorCode = request.getSessionName();
        person(request, operatorCode);
    }
        
    public static void person(HttpRequest request, String operatorCode) throws Exception
    {
        //第一步，从页面中读出当前年月，并根据上下月参数找到对应的年月
        int year = request.getParameterInt("year", DateTimes.getCurrentYear());
        int month = request.getParameterInt("month", DateTimes.getCurrentMonth());
        
        boolean isPrev = request.getParameterBoolean("prev");
        boolean isNext = request.getParameterBoolean("next");
        
        if (isPrev)
        {
            year = month==1?year-1:year;
            month = month==1?12:month-1;
        }
        else if (isNext)
        {
            year = month==12?year+1:year;
            month = month==12?1:month+1;
        }
        
        //第二步，查出上个月，当前月和下个月的个人计划&总结
        int prevYear = month==1?year-1:year;
        int prevMonth = month==1?12:month-1;
        
        int nextYear = month==12?year+1:year;
        int nextMonth = month==12?1:month+1;
        
        Selector selector = new Selector();
        selector.addMust("operatorCode", operatorCode);
        selector.addMustThenG("planDate", prevYear * 10000 + prevMonth * 100);
        selector.addMustThenL("planDate", nextYear * 10000 + (nextMonth+1) * 100);
        List<GitPersonPlan> planList = ORM.get(ZTable.class, request).list(GitPersonPlan.class, selector);
        
        Selector selector2 = new Selector();
        selector2.addMust("operatorCode", operatorCode);
        selector2.addMustThenG("summaryDate", prevYear * 10000 + prevMonth * 100);
        selector2.addMustThenL("summaryDate", nextYear * 10000 + (nextMonth+1) * 100);
        List<GitPersonSummary> summaryList = ORM.get(ZTable.class, request).list(GitPersonSummary.class, selector2);
        
        //第三步，组装有效周列表
        List<PersonWeekModel> weekList = new ArrayList<>();
        for (CalendarWeekModel week : GitCalendarDao.getCalendarWeekList(request, year, month))
        {
            weekList.add(new PersonWeekModel(week));
        }
        
        /******************************************************************************/
        //以下为内容填充
        /******************************************************************************/
        
        //第四步，找到周下面所有的计划
        for (PersonWeekModel week : weekList)
        {
            CalendarWeekModel key = week.getKey();
            List<GitPersonPlan> pList = getPlan(planList, key.getBeginDate(), key.getEndDate());
            if (pList.isEmpty())
            {//没有计划，则插入一个空项计划
                PersonPlanModel model = new PersonPlanModel();
                model.setDate(key.getBeginDate());
                model.setSeq(1);
                
                week.addPlanModel(model);
            }
            else
            {//有则用计划表
                for (GitPersonPlan plan : pList)
                {
                    PersonPlanModel model = new PersonPlanModel();
                    model.setId(plan.getPlanId());
                    model.setDate(plan.getPlanDate());
                    model.setSeq(plan.getPlanSeq());
                    model.setStatus(plan.getPlanStatus());
                    model.setContent(plan.getPlanContent());
                    
                    week.addPlanModel(model);
                }
            }
        }
        
        //第五步，生成日总结
        for (PersonWeekModel week : weekList)
        {
            CalendarWeekModel key = week.getKey();
            int beginDate = key.getBeginDate();
            int endDate = key.getEndDate();
            for (int date=beginDate;date<=endDate;date=DateTimes.getNextDateInt(date))
            {
                PersonDayModel model = new PersonDayModel();
                model.setDate(date);
                model.setWeek(DateTimes.getDateWeek7(date));
                
                GitPersonSummary summary = getPersonSummary(summaryList, date, PS_SUM_DAY);
                if (summary != null)
                {
                    model.setId(summary.getSummaryId());
                    model.setContent(summary.getSummaryContent());
                }
                
                week.addDayModel(model);
            }
        }
        
        //第六步，生成周总结
        for (PersonWeekModel week : weekList)
        {
            CalendarWeekModel key = week.getKey();
            PersonModel model = new PersonModel();
            model.setType(PS_SUM_WEEK);
            model.setDate(key.getBeginDate());
            
            GitPersonSummary summary = getWeekSummary(summaryList, key.getBeginDate(), key.getEndDate());
            if (summary != null)
            {
                model.setId(summary.getSummaryId());
                model.setContent(summary.getSummaryContent());
            }
            
            week.setWeek(model);
        }
        
        //第七步，生成月总结
        int monthDate = year * 10000 + month * 100 + 1;
        
        PersonModel monthModel = new PersonModel();
        monthModel.setType(PS_SUM_MONTH);
        monthModel.setDate(monthDate);
        
        GitPersonSummary summary = getPersonSummary(summaryList, monthDate, PS_SUM_MONTH);
        if (summary != null)
        {
            monthModel.setId(summary.getSummaryId());
            monthModel.setContent(summary.getSummaryContent());
        }
        
        request.setAttribute("year", year);
        request.setAttribute("month", month);
        request.setAttribute("weekList", weekList);
        request.setAttribute("monthModel", monthModel);
    }
    
    /*********************************************************************************************************/
    //内部实现方法
    /*********************************************************************************************************/
    
    /** 找出本周的计划 */
    private static List<GitPersonPlan> getPlan(List<GitPersonPlan> planList, int beginDate, int endDate)
    {
        List<GitPersonPlan> pList = new ArrayList<>();
        for (GitPersonPlan item : planList)
        {
            if (item.getPlanDate() < beginDate || item.getPlanDate() > endDate)
                continue;
            
            pList.add(item);
        }
        
        return pList;
    }
    
    /** 找出本周的总结 */
    private static GitPersonSummary getWeekSummary(List<GitPersonSummary> planList, int beginDate, int endDate)
    {
        for (GitPersonSummary item : planList)
        {
            if (item.getSummaryType() != PS_SUM_WEEK)
                continue;
            
            if (item.getSummaryDate() < beginDate || item.getSummaryDate() > endDate)
                continue;
            
            return item;
        }
        
        return null;
    }
    
    /** 找出类型和日期对应的总结 */
    private static GitPersonSummary getPersonSummary(List<GitPersonSummary> summaryList, int date, int type)
    {
        for (GitPersonSummary item : summaryList)
        {
            if (item.getSummaryDate() == date && item.getSummaryType() == type)
                return item;
        }
        
        return null;
    }
}
