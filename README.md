### 什么是“gitcan”？
---------------------------------------
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;“gitcan”是一套简洁的项目管理软件，集成了代码托管gitsolo，项目需求、BUG、缺陷管理，项目计划和个人计划总结，设备信息管理等功能，整合了开发、测试、发布和运维等软件项目开发流程。适用于中小软件公司在自己的环境下搭建和实现项目管理跟踪。

<br>

### gitcan有什么优点？
---------------------------------------
1、想搭一个Git服务器，又觉得GitLab很麻烦的公司或个人，选择gitsolo/gitcan绝对错不了。<br>
2、gitsolo/gitcan只依赖JDK和ZhiqimDK，安装简单到爆，特别是有经验的Java程序员，配置好boot.home，一键启动（zhiqim.exe/zhiqim.lix）。<br>
3、独立工程，不需要WEB容器，什么Tomcat/Jetty不需要的。<br>
4、有HTTP(S)协议就够了，什么ssh协议配置太麻烦。gitsolo/gitcan的多项目/多成员管理很棒的。<br>
5、还有一个特点就是会检查提交者和提交者邮箱，这个要注意啦，提交者必须是gitsolo/gitcan的用户名，否则提交不了，为什么这么设计呢？因为这么控制了之后，可以避免别人用自己的账号提交啦，哈哈哈。<br>
6、在gitsolo基于上，gitcan新增了模块功能管理，项目计划总结，项目BUG跟踪管理，项目设备管理和个人计划总结和个人待办事项等功能，非常方便小团队和小微软件公司日常管理
<br>

### gitcan安装指南
---------------------------------------
1、要求JDK1.7+,其详细安装教程请前往[【JDK安装教程】](https://zhiqim.org/document/bestcase/jdk.htm)。<br>
2、进往下载发行版：下载请点击[【gitcan发行版】](https://zhiqim.org/project/zhiqim_products/gitcan/release.htm)。<br>
<br>

### 知启蒙技术框架与交流
---------------------------------------
![知启蒙技术框架架构图](https://zhiqim.org/project/images/101431_93f5c39d_2103954.jpeg "知启蒙技术框架架构图.jpg")<br><br>
QQ群：加入QQ交流群，请点击[【458171582】](https://jq.qq.com/?_wv=1027&k=5DWlB3b) <br><br>
教程：欲知更多gitcan，[【请戳这里】](https://zhiqim.org/project/zhiqim_products/gitcan/tutorial/index.htm)
