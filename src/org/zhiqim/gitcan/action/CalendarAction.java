/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.action;

import java.util.ArrayList;
import java.util.List;

import org.zhiqim.gitcan.GitcanConstants;
import org.zhiqim.gitcan.dbo.GitCalendar;
import org.zhiqim.gitcan.model.CalendarModel;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.kernel.util.DateTimes;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;

/**
 * 日历信息表
 *
 * @version v1.0.0 @author huangsufang 2017-10-27 新建与整理
 */
public class CalendarAction implements Action, GitcanConstants
{
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        int year = request.getParameterInt("year", DateTimes.getCurrentYear());
        int month = request.getParameterInt("month", DateTimes.getCurrentMonth());
        
        boolean isPrev = request.getParameterBoolean("prev");
        boolean isNext = request.getParameterBoolean("next");
        
        if (isPrev)
        {
            year = month==1?year-1:year;
            month = month==1?12:month-1;
        }
        else if (isNext)
        {
            year = month==12?year+1:year;
            month = month==12?1:month+1;
        }
        
        //计算上月和下月，并取出上当下三月数据
        int prevYear = month==1?year-1:year;
        int prevMonth = month==1?12:month-1;
        int nextYear = month==12?year+1:year;
        int nextMonth = month==12?1:month+1;

        Selector selector = new Selector();
        selector.addMustThenG("calendarDate", prevYear * 10000 + prevMonth * 100);
        selector.addMustThenL("calendarDate", nextYear * 10000 + (nextMonth+1) * 100);
        List<GitCalendar> calendarList = ORM.get(ZTable.class, request).list(GitCalendar.class, selector);
        
        List<CalendarModel> list = new ArrayList<>();
        
        //生成日历表
        int firstWeek = DateTimes.getDateWeek7(year, month, 1); //某月第一天的星期几
        int curMonthMaxDay = DateTimes.getMonthDays(year, month);
        int prevMonthMaxDay = DateTimes.getMonthDays(prevYear, prevMonth);
        
        //1到firstWeek表示上月
        for (int i=1;i<=firstWeek;i++)
        {
            int day = prevMonthMaxDay-firstWeek+i;
            
            CalendarModel date = new CalendarModel();
            date.setCalendarDate(prevYear, prevMonth, day);
            date.setCalendarMonth(PREV_MONTH);
            
            setCalendarDay(calendarList, date);
            list.add(date);
        }
        
        //firstWeek+1到firstWeek+1+curMonthMaxDay表示当月
        for (int i=firstWeek+1;i<firstWeek+1+curMonthMaxDay;i++)
        {
            int day = i-firstWeek;
            
            CalendarModel date = new CalendarModel();
            date.setCalendarDate(year, month, day);
            date.setCalendarMonth(CURR_MONTH);
            
            setCalendarDay(calendarList, date);
            list.add(date);
        }
        
        //firstWeek+1+curMonthMaxDay+1到42表示下月
        for (int i=firstWeek+1+curMonthMaxDay;i<=42;i++)
        {
            int day = i-curMonthMaxDay-firstWeek;//日期从1到显示的最后一天
            
            CalendarModel date = new CalendarModel();
            date.setCalendarDate(nextYear, nextMonth, day);
            date.setCalendarMonth(NEXT_MONTH);
            
            setCalendarDay(calendarList, date);
            list.add(date);
        }
        
        request.setAttribute("year", year);
        request.setAttribute("month", month);
        request.setAttribute("list", list);
    }
    
    private void setCalendarDay(List<GitCalendar> calendarList, CalendarModel date)
    {
        GitCalendar calendar = getCalendar(calendarList, date.getCalendarDate());
        if (calendar != null)
        {//存在配置取配置
            date.setCalendarRest(calendar.isCalendarRest());
        }
        else
        {//否则周六周末认为是休息日
            int week = DateTimes.getDateWeek7(date.getYear(), date.getMonth(), date.getDay());
            date.setCalendarRest(week == 6 || week == 7);
        }
    }
    
    private GitCalendar getCalendar(List<GitCalendar> calendarList, int date)
    {
        for (GitCalendar calendar : calendarList)
        {
            if (calendar.getCalendarDate() == date)
                return calendar;
        }
        
        return null;
    }
}
