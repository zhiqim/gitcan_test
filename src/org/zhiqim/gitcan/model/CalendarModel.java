/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.model;

/**
 * 日历模型，包括日期所在上月/当前/下月,日期和是否休息日
 *
 * @version v1.0.0 @author zouzhigang 2017-11-8 新建与整理
 */
public class CalendarModel
{
    private String calendarMonth;
    private int calendarDate;
    private boolean calendarRest;
    
    private int year;
    private int month;
    private int day;
    
    /****************************************************************/
    //设置属性
    /****************************************************************/

    public void setCalendarMonth(String calendarMonth)
    {
        this.calendarMonth = calendarMonth;
    }

    public void setCalendarRest(boolean calendarRest)
    {
        this.calendarRest = calendarRest;
    }
    
    public void setCalendarDate(int year, int month, int day)
    {
        this.calendarDate = year * 10000 + month * 100 + day;
        
        this.year = year;
        this.month = month;
        this.day = day;
    }
    
    public void setCalendarDate(int calendarDate)
    {
        this.calendarDate = calendarDate;
        
        this.year = calendarDate / 10000;
        this.month = calendarDate / 100 % 100;
        this.day = calendarDate % 100;
    }
    
    /****************************************************************/
    //获取属性
    /****************************************************************/
    
    public String getCalendarMonth()
    {
        return calendarMonth;
    }
    
    public boolean isCalendarRest()
    {
        return calendarRest;
    }
    
    public int getCalendarDate()
    {
        return calendarDate;
    }
    
    public int getYear()
    {
        return year;
    }

    public int getMonth()
    {
        return month;
    }

    public int getDay()
    {
        return day;
    }
}
