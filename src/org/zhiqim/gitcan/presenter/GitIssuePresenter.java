/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.presenter;

import org.zhiqim.gitcan.GitcanConstants;
import org.zhiqim.gitcan.dbo.GitIssue;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.util.Sqls;
import org.zhiqim.kernel.util.Validates;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Updater;

/**
 * 项目问题功能展示器
 *
 * @version v1.0.0 @author zouzhigang 2017-11-7 新建与整理
 */
@AnAlias("GitIssuePresenter")
public class GitIssuePresenter implements GitcanConstants
{
    /**
     * 指派问题（BUG/需求/缺陷）
     * 
     * @param request       请求
     * @param issueId       问题编号
     * @param operatorCode  操作员编码
     * @throws Exception    异常
     */
    public static void doAssignIssue(HttpRequest request, long issueId, String operatorCode) throws Exception
    {
        GitIssue issue = ORM.get(ZTable.class, request).item(GitIssue.class, issueId);
        if (issue == null)
        {
            request.setResponseError("问题不存在");
            return;
        }

        Updater updater = new Updater();
        updater.addMust("issueId", issueId);
        updater.addField("issueManager", operatorCode);
        updater.addField("issueStatus", HANDLING);
        updater.addField("issueModified", Sqls.nowTimestamp());
        ORM.get(ZTable.class, request).update(GitIssue.class, updater);
        
        request.setResponseResult("指派成功");
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "指派了问题", issue.getIssueSeq(), operatorCode);
    }
    
    
    /**
     * 处理问题（BUG/需求/缺陷）
     * 
     * @param request       请求
     * @param issueId       问题编号
     * @param status        问题状态
     * @param issueSolution 问题解决方案
     * @throws Exception    异常
     */
    public static void doSolveIssue(HttpRequest request, long issueId, String status, String issueSolution) throws Exception
    {
        GitIssue issue = ORM.get(ZTable.class, request).item(GitIssue.class, issueId);
        if (issue == null)
        {
            request.setResponseError("问题不存在");
            return;
        }
        
        Updater updater = new Updater();
        updater.addMust("issueId", issueId);
        
        if (REJECT.equals(status) && (CREATED.equals(issue.getIssueStatus()) || HANDLING.equals(issue.getIssueStatus())))
        {//新创建&处理中时，负责人打回创建者，要把负责人修改成创建者
            updater.addField("issueManager", issue.getIssueCreator());
        }
        
        updater.addField("issueStatus", status);
        updater.addField("issueModified", Sqls.nowTimestamp());
        if (Validates.isNotEmpty(issueSolution))
            updater.addField("issueSolution", issueSolution);
        ORM.get(ZTable.class, request).update(GitIssue.class, updater);
        
        request.setResponseResult("处理成功");
        
        GitMemberDao.report(request, request.getSessionName(), GitProjectDao.getProjectId(request), status, "问题", issue.getIssueSeq());
    }
}