/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.action;

import org.zhiqim.gitcan.dbo.GitCalendar;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.ValidateAction;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;

/**
 * 日历休息日/工作日数据删除
 *
 * @version v1.0.0 @author zhichenggang 2014-3-21 新建与整理
 */
public class CalendarDeleteAction extends ValidateAction
{
    @Override
    protected void validate(HttpRequest request)
    {
        request.addValidate(new IsNotEmpty("calendarDate", "请选择一个选项"));
    }

    @Override
    protected void perform(HttpRequest request) throws Exception
    {
        int calendarDate = request.getParameterInt("calendarDate");
        GitCalendar item = ORM.get(ZTable.class, request).item(GitCalendar.class, calendarDate);
        if(item == null)
        {
            request.returnHistory("日历不存在，请重新选择");
            return;
        }
        
        ORM.get(ZTable.class, request).delete(GitCalendar.class, calendarDate);
    }
}
