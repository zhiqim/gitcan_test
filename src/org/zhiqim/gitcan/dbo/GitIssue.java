/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;
import java.sql.Timestamp;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目问题表 对应表《GIT_ISSUE》
 */
@AnAlias("GitIssue")
@AnNew
@AnTable(table="GIT_ISSUE", key="ISSUE_ID", type="InnoDB")
public class GitIssue implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="ISSUE_ID", type="long", notNull=true)    private long issueId;    //2.问题编号
    @AnTableField(column="ISSUE_TYPE", type="byte", notNull=true)    private int issueType;    //3.问题类型，1：需求，2：BUG，3：缺陷
    @AnTableField(column="ISSUE_NAME", type="string,50", notNull=true)    private String issueName;    //4.问题名称
    @AnTableField(column="ISSUE_STATUS", type="string,10", notNull=true)    private String issueStatus;    //5.问题状态，created：已创建  handling：处理中  hangup：挂起  completed：已解决 closed：已关闭 reject：已打回
    @AnTableField(column="ISSUE_SEQ", type="int", notNull=true)    private int issueSeq;    //6.问题排序
    @AnTableField(column="ISSUE_PRIORITY", type="byte", notNull=true)    private int issuePriority;    //7.问题紧急程度，3：紧急，2：较急，1，不急
    @AnTableField(column="ISSUE_MANAGER", type="string,32", notNull=true)    private String issueManager;    //8.问题负责人
    @AnTableField(column="ISSUE_CREATOR", type="string,32", notNull=true)    private String issueCreator;    //9.问题创建者
    @AnTableField(column="ISSUE_CREATED", type="datetime", notNull=true)    private Timestamp issueCreated;    //10.创建时间
    @AnTableField(column="ISSUE_MODIFIED", type="datetime", notNull=true)    private Timestamp issueModified;    //11.更新时间
    @AnTableField(column="ISSUE_DESC", type="string,1024", notNull=false)    private String issueDesc;    //12.问题描述
    @AnTableField(column="ISSUE_CAUSE", type="string,1024", notNull=false)    private String issueCause;    //13.问题分析
    @AnTableField(column="ISSUE_SOLUTION", type="string,1024", notNull=false)    private String issueSolution;    //14.问题解决方案

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public long getIssueId()
    {
        return issueId;
    }

    public void setIssueId(long issueId)
    {
        this.issueId = issueId;
    }

    public int getIssueType()
    {
        return issueType;
    }

    public void setIssueType(int issueType)
    {
        this.issueType = issueType;
    }

    public String getIssueName()
    {
        return issueName;
    }

    public void setIssueName(String issueName)
    {
        this.issueName = issueName;
    }

    public String getIssueStatus()
    {
        return issueStatus;
    }

    public void setIssueStatus(String issueStatus)
    {
        this.issueStatus = issueStatus;
    }

    public int getIssueSeq()
    {
        return issueSeq;
    }

    public void setIssueSeq(int issueSeq)
    {
        this.issueSeq = issueSeq;
    }

    public int getIssuePriority()
    {
        return issuePriority;
    }

    public void setIssuePriority(int issuePriority)
    {
        this.issuePriority = issuePriority;
    }

    public String getIssueManager()
    {
        return issueManager;
    }

    public void setIssueManager(String issueManager)
    {
        this.issueManager = issueManager;
    }

    public String getIssueCreator()
    {
        return issueCreator;
    }

    public void setIssueCreator(String issueCreator)
    {
        this.issueCreator = issueCreator;
    }

    public Timestamp getIssueCreated()
    {
        return issueCreated;
    }

    public void setIssueCreated(Timestamp issueCreated)
    {
        this.issueCreated = issueCreated;
    }

    public Timestamp getIssueModified()
    {
        return issueModified;
    }

    public void setIssueModified(Timestamp issueModified)
    {
        this.issueModified = issueModified;
    }

    public String getIssueDesc()
    {
        return issueDesc;
    }

    public void setIssueDesc(String issueDesc)
    {
        this.issueDesc = issueDesc;
    }

    public String getIssueCause()
    {
        return issueCause;
    }

    public void setIssueCause(String issueCause)
    {
        this.issueCause = issueCause;
    }

    public String getIssueSolution()
    {
        return issueSolution;
    }

    public void setIssueSolution(String issueSolution)
    {
        this.issueSolution = issueSolution;
    }

}
