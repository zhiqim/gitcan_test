/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 远程控制表 对应表《GIT_CONTROL》
 */
@AnAlias("GitControl")
@AnNew
@AnTable(table="GIT_CONTROL", key="CONTROL_ID", type="InnoDB")
public class GitControl implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="CONTROL_ID", type="long", notNull=true)    private long controlId;    //2.控制编号
    @AnTableField(column="CONTROL_NAME", type="string,128", notNull=true)    private String controlName;    //3.控制名称
    @AnTableField(column="CONTROL_DOMAIN", type="string,128", notNull=true)    private String controlDomain;    //4.控制域名或IP
    @AnTableField(column="CONTROL_PORT", type="int", notNull=true)    private int controlPort;    //5.控制端口
    @AnTableField(column="CONTROL_USER", type="string,32", notNull=true)    private String controlUser;    //6.控制管理员账户
    @AnTableField(column="CONTROL_PASSWORD", type="string,32", notNull=true)    private String controlPassword;    //7.控制管理员密码

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public long getControlId()
    {
        return controlId;
    }

    public void setControlId(long controlId)
    {
        this.controlId = controlId;
    }

    public String getControlName()
    {
        return controlName;
    }

    public void setControlName(String controlName)
    {
        this.controlName = controlName;
    }

    public String getControlDomain()
    {
        return controlDomain;
    }

    public void setControlDomain(String controlDomain)
    {
        this.controlDomain = controlDomain;
    }

    public int getControlPort()
    {
        return controlPort;
    }

    public void setControlPort(int controlPort)
    {
        this.controlPort = controlPort;
    }

    public String getControlUser()
    {
        return controlUser;
    }

    public void setControlUser(String controlUser)
    {
        this.controlUser = controlUser;
    }

    public String getControlPassword()
    {
        return controlPassword;
    }

    public void setControlPassword(String controlPassword)
    {
        this.controlPassword = controlPassword;
    }

}
