/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.presenter;

import org.zhiqim.gitcan.dbo.GitBackend;
import org.zhiqim.gitcan.dbo.GitControl;
import org.zhiqim.gitcan.dbo.GitDatabase;
import org.zhiqim.gitcan.dbo.GitMachine;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.annotation.AnIntercept;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.httpd.validate.onex.IsInteger;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.manager.ZmrBootstrap;
import org.zhiqim.manager.ZmrPassworder;
import org.zhiqim.manager.ZmrSessionUser;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;


@AnAlias("GitEquipmentPresenter")
@AnIntercept("chkZmrLogin")
public class GitEquipmentPresenter
{
    /**
     * 获取服务器密码，要求验证操作员密码
     * 
     * @param request       请求
     * @throws Exception    异常
     */
    public static void getMachinePasswordValidate(HttpRequest request) throws Exception
    {
        request.addValidate(new IsNotEmpty("operatorPass", "密码不能为空"));
        request.addValidate(new IsInteger("machineId", "请选择服务器"));
        if (!request.chkValidate())
        {
            request.setResponseError(request.getAlertMsg());
            return;
        }
        
        String operatorPass = request.getParameter("operatorPass");
        if (!validatePassword(request, operatorPass))
        {//密码不正确
            return;
        }
        
        long machineId = request.getParameterLong("machineId");
        GitMachine machine = ORM.get(ZTable.class, request).item(GitMachine.class, machineId);
        if (machine == null)
        {
            request.setResponseError("服务器不存在");
            return;
        }
        
        request.setResponseResult(machine.getMachinePassword());
    }
    
    /**
     * 获取数据库密码，要求验证操作员密码
     * 
     * @param request       请求
     * @throws Exception    异常
     */
    public static void getDatabasePasswordValidate(HttpRequest request) throws Exception
    {
        request.addValidate(new IsNotEmpty("operatorPass", "密码不能为空"));
        request.addValidate(new IsInteger("databaseId", "请选择数据库"));
        if (!request.chkValidate())
        {
            request.setResponseError(request.getAlertMsg());
            return;
        }
        
        String operatorPass = request.getParameter("operatorPass");
        if (!validatePassword(request, operatorPass))
        {//密码不正确
            return;
        }
        
        long databaseId = request.getParameterLong("databaseId");
        GitDatabase database = ORM.get(ZTable.class, request).item(GitDatabase.class, databaseId);
        if (database == null)
        {
            request.setResponseError("数据库不存在");
            return;
        }
        
        request.setResponseResult(database.getDatabasePassword());
    }
        
    /**
     * 获取后台密码，要求验证操作员密码
     * 
     * @param request       请求
     * @throws Exception    异常
     */
    public static void getBackendPasswordValidate(HttpRequest request) throws Exception
    {
        request.addValidate(new IsNotEmpty("operatorPass", "密码不能为空"));
        request.addValidate(new IsInteger("backendId", "请选择后台"));
        if (!request.chkValidate())
        {
            request.setResponseError(request.getAlertMsg());
            return;
        }
        
        String operatorPass = request.getParameter("operatorPass");
        if (!validatePassword(request, operatorPass))
        {//密码不正确
            return;
        }
        
        long backendId = request.getParameterLong("backendId");
        GitBackend backend = ORM.get(ZTable.class, request).item(GitBackend.class, backendId);
        if (backend == null)
        {
            request.setResponseError("后台不存在");
            return;
        }
        
        request.setResponseResult(backend.getBackendPassword());
    }
        
    /**
     * 获取控制台密码，要求验证操作员密码
     * 
     * @param request       请求
     * @throws Exception    异常
     */
    public static void getControlPasswordValidate(HttpRequest request) throws Exception
    {
        request.addValidate(new IsNotEmpty("operatorPass", "密码不能为空"));
        request.addValidate(new IsInteger("controlId", "请选择远程控制"));
        if (!request.chkValidate())
        {
            request.setResponseError(request.getAlertMsg());
            return;
        }
        
        String operatorPass = request.getParameter("operatorPass");
        if (!validatePassword(request, operatorPass))
        {//密码不正确
            return;
        }

        long controlId = request.getParameterLong("controlId");
        GitControl control = ORM.get(ZTable.class, request).item(GitControl.class, controlId);
        if (control == null)
        {
            request.setResponseError("远程控制不存在");
            return;
        }
        
        request.setResponseResult(control.getControlPassword());
    }
    
    /**
     * 验证操作员密码
     * 
     * @param request       请求
     * @param operatorPass  密码
     * @return              =true表示成功
     */
    private static boolean validatePassword(HttpRequest request, String operatorPass)
    {
        ZmrSessionUser sessionUser = request.getSessionUser(ZmrSessionUser.class);
        String operatorCode = sessionUser.getOperatorCode();
        
        ZmrPassworder passworder = ZmrBootstrap.getPassworder();
        String oldPassEncode = passworder.encode(operatorCode, operatorPass, sessionUser.getOperatorPassSalt());
        if(!oldPassEncode.equalsIgnoreCase(sessionUser.getOperator().getOperatorPass()))
        {
            request.setResponseError("密码不正确");
            return false;
        }
        
        return true;
    }
}
