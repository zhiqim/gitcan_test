/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目需求表 对应表《GIT_REQUIREMENT》
 */
@AnAlias("GitRequirement")
@AnNew
@AnTable(table="GIT_REQUIREMENT", key="ISSUE_ID", type="InnoDB")
public class GitRequirement implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="ISSUE_ID", type="long", notNull=true)    private long issueId;    //1.问题编号
    @AnTableField(column="REQUIREMENT_IMAGE", type="binary", notNull=false)    private byte[] requirementImage;    //2.需求图片
    @AnTableField(column="REQUIREMENT_FILE", type="string,128", notNull=false)    private String requirementFile;    //3.需求附件

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getIssueId()
    {
        return issueId;
    }

    public void setIssueId(long issueId)
    {
        this.issueId = issueId;
    }

    public byte[] getRequirementImage()
    {
        return requirementImage;
    }

    public void setRequirementImage(byte[] requirementImage)
    {
        this.requirementImage = requirementImage;
    }

    public String getRequirementFile()
    {
        return requirementFile;
    }

    public void setRequirementFile(String requirementFile)
    {
        this.requirementFile = requirementFile;
    }

}
