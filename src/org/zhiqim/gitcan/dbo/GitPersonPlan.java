/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;
import java.sql.Timestamp;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 个人计划表 对应表《GIT_PERSON_PLAN》
 */
@AnAlias("GitPersonPlan")
@AnNew
@AnTable(table="GIT_PERSON_PLAN", key="PLAN_ID", type="InnoDB")
public class GitPersonPlan implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="OPERATOR_CODE", type="string,32", notNull=true)    private String operatorCode;    //1.操作员编号
    @AnTableField(column="PLAN_ID", type="long", notNull=true)    private long planId;    //2.计划编号
    @AnTableField(column="PLAN_DATE", type="int", notNull=true)    private int planDate;    //3.计划日期，格式yyyyMMdd，如20171101
    @AnTableField(column="PLAN_STATUS", type="byte", notNull=true)    private int planStatus;    //4.计划状态，0：正常，1：新增，2：延误，3：取消
    @AnTableField(column="PLAN_SEQ", type="byte", notNull=true)    private int planSeq;    //5.计划序号，从1开始，周内排序
    @AnTableField(column="PLAN_MODIFIED", type="datetime", notNull=true)    private Timestamp planModified;    //6.计划最后更新时间
    @AnTableField(column="PLAN_CONTENT", type="string,256", notNull=false)    private String planContent;    //7.计划内容

    public String toString()
    {
        return Jsons.toString(this);
    }

    public String getOperatorCode()
    {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode)
    {
        this.operatorCode = operatorCode;
    }

    public long getPlanId()
    {
        return planId;
    }

    public void setPlanId(long planId)
    {
        this.planId = planId;
    }

    public int getPlanDate()
    {
        return planDate;
    }

    public void setPlanDate(int planDate)
    {
        this.planDate = planDate;
    }

    public int getPlanStatus()
    {
        return planStatus;
    }

    public void setPlanStatus(int planStatus)
    {
        this.planStatus = planStatus;
    }

    public int getPlanSeq()
    {
        return planSeq;
    }

    public void setPlanSeq(int planSeq)
    {
        this.planSeq = planSeq;
    }

    public Timestamp getPlanModified()
    {
        return planModified;
    }

    public void setPlanModified(Timestamp planModified)
    {
        this.planModified = planModified;
    }

    public String getPlanContent()
    {
        return planContent;
    }

    public void setPlanContent(String planContent)
    {
        this.planContent = planContent;
    }

}
