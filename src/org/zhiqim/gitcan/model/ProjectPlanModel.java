/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.model;

/**
 * 项目计划模型
 *
 * @version v1.0.0 @author zouzhigang 2017-11-8 新建与整理
 */
public class ProjectPlanModel extends ProjectModel
{
    private int planSeq;
    private int planStatus;
    
    private String planBeginDate;
    private String planEndDate;
    private String planManager;
    private int planProgress;
    private String actualBeginDate;
    private String actualEndDate;

    
    public int getType()
    {
        return PJ_PLAN;
    }
    
    public int getPlanSeq()
    {
        return planSeq;
    }

    public void setPlanSeq(int planSeq)
    {
        this.planSeq = planSeq;
    }

    public int getPlanStatus()
    {
        return planStatus;
    }

    public void setPlanStatus(int planStatus)
    {
        this.planStatus = planStatus;
    }

    public String getPlanBeginDate()
    {
        return planBeginDate;
    }

    public void setPlanBeginDate(String planBeginDate)
    {
        this.planBeginDate = planBeginDate;
    }

    public String getPlanEndDate()
    {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate)
    {
        this.planEndDate = planEndDate;
    }

    public String getPlanManager()
    {
        return planManager;
    }

    public void setPlanManager(String planManager)
    {
        this.planManager = planManager;
    }

    public int getPlanProgress()
    {
        return planProgress;
    }

    public void setPlanProgress(int planProgress)
    {
        this.planProgress = planProgress;
    }

    public String getActualBeginDate()
    {
        return actualBeginDate;
    }

    public void setActualBeginDate(String actualBeginDate)
    {
        this.actualBeginDate = actualBeginDate;
    }

    public String getActualEndDate()
    {
        return actualEndDate;
    }

    public void setActualEndDate(String actualEndDate)
    {
        this.actualEndDate = actualEndDate;
    }

    public String getColor()
    {
        switch (planStatus)
        {
        case 1: return "color:green";
        case 2: return "color:red";
        case 3: return "color:gray";
        default: return "color:#333";
        }
    }
}
