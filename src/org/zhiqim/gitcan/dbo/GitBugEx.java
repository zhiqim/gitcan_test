/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import org.zhiqim.gitcan.dbo.GitIssue;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * BUG扩展视图 对应视图《GIT_BUG_EX》
 */
@AnAlias("GitBugEx")
@AnNew
@AnView("GIT_BUG,GIT_ISSUE,GIT_MODULE")
@AnViewJoin({@AnViewJoinValue(type="EQUAL", lTable="GIT_BUG", lColumn="ISSUE_ID", rTable="GIT_ISSUE", rColumn="ISSUE_ID"),
             @AnViewJoinValue(type="EQUAL", lTable="GIT_BUG", lColumn="MODULE_ID", rTable="GIT_MODULE", rColumn="MODULE_ID")})
public class GitBugEx extends GitIssue
{
    private static final long serialVersionUID = 1L;

    @AnViewField(table="GIT_BUG", column="MODULE_ID")    private long moduleId;    //2.模块编号
    @AnViewField(table="GIT_MODULE", column="MODULE_NAME")    private String moduleName;    //3.模块名称
    @AnViewField(table="GIT_BUG", column="BUG_LEVEL")    private int bugLevel;    //4.BUG严重程度，3：致命，2：严重，1：轻微
    @AnViewField(table="GIT_BUG", column="BUG_IMAGE")    private byte[] bugImage;    //5.BUG图片

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getModuleId()
    {
        return moduleId;
    }

    public void setModuleId(long moduleId)
    {
        this.moduleId = moduleId;
    }

    public String getModuleName()
    {
        return moduleName;
    }

    public void setModuleName(String moduleName)
    {
        this.moduleName = moduleName;
    }

    public int getBugLevel()
    {
        return bugLevel;
    }

    public void setBugLevel(int bugLevel)
    {
        this.bugLevel = bugLevel;
    }

    public byte[] getBugImage()
    {
        return bugImage;
    }

    public void setBugImage(byte[] bugImage)
    {
        this.bugImage = bugImage;
    }

}
