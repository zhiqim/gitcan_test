/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 后台管理表 对应表《GIT_BACKEND》
 */
@AnAlias("GitBackend")
@AnNew
@AnTable(table="GIT_BACKEND", key="BACKEND_ID", type="InnoDB")
public class GitBackend implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="BACKEND_ID", type="long", notNull=true)    private long backendId;    //2.后台编号
    @AnTableField(column="BACKEND_NAME", type="string,64", notNull=true)    private String backendName;    //3.后台名称
    @AnTableField(column="BACKEND_URL", type="string,128", notNull=true)    private String backendUrl;    //4.后台地址
    @AnTableField(column="BACKEND_USER", type="string,32", notNull=true)    private String backendUser;    //5.后台管理员账户
    @AnTableField(column="BACKEND_PASSWORD", type="string,32", notNull=true)    private String backendPassword;    //6.后台管理员密码

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public long getBackendId()
    {
        return backendId;
    }

    public void setBackendId(long backendId)
    {
        this.backendId = backendId;
    }

    public String getBackendName()
    {
        return backendName;
    }

    public void setBackendName(String backendName)
    {
        this.backendName = backendName;
    }

    public String getBackendUrl()
    {
        return backendUrl;
    }

    public void setBackendUrl(String backendUrl)
    {
        this.backendUrl = backendUrl;
    }

    public String getBackendUser()
    {
        return backendUser;
    }

    public void setBackendUser(String backendUser)
    {
        this.backendUser = backendUser;
    }

    public String getBackendPassword()
    {
        return backendPassword;
    }

    public void setBackendPassword(String backendPassword)
    {
        this.backendPassword = backendPassword;
    }

}
