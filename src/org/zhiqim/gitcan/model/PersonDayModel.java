/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.model;

/**
 * 个人日总结模型，在个人计划总结模型上增加星期
 *
 * @version v1.0.0 @author zouzhigang 2017-11-8 新建与整理
 */
public class PersonDayModel extends PersonModel
{
    private int week;
    
    public int getType()
    {
        return PS_SUM_DAY;
    }

    public int getWeek()
    {
        return week;
    }

    public void setWeek(int week)
    {
        this.week = week;
    }
    
    public String getWeekString()
    {
        switch (week)
        {
        case 1:return "周一";
        case 2:return "周二";
        case 3:return "周三";
        case 4:return "周四";
        case 5:return "周五";
        case 6:return "周六";
        case 7:return "周日";
        }
        
        return null;
    }
}
