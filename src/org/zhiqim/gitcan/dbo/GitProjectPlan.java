/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;
import java.sql.Timestamp;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目计划表 对应表《GIT_PROJECT_PLAN》
 */
@AnAlias("GitProjectPlan")
@AnNew
@AnTable(table="GIT_PROJECT_PLAN", key="PLAN_ID", type="InnoDB")
public class GitProjectPlan implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="PLAN_ID", type="long", notNull=true)    private long planId;    //2.计划编号
    @AnTableField(column="PLAN_DATE", type="int", notNull=true)    private int planDate;    //3.计划日期，格式yyyyMMdd，如20171101
    @AnTableField(column="PLAN_STATUS", type="byte", notNull=true)    private int planStatus;    //4.计划状态，0：正常，1：新增，2：延误，3：取消
    @AnTableField(column="PLAN_SEQ", type="byte", notNull=true)    private int planSeq;    //5.计划序号，从1开始，周内排序
    @AnTableField(column="PLAN_PROGRESS", type="byte", notNull=true)    private int planProgress;    //6.计划进度，0-100
    @AnTableField(column="PLAN_MODIFIED", type="datetime", notNull=true)    private Timestamp planModified;    //7.计划更新时间
    @AnTableField(column="PLAN_CONTENT", type="string,128", notNull=false)    private String planContent;    //8.计划内容
    @AnTableField(column="PLAN_MANAGER", type="string,32", notNull=false)    private String planManager;    //9.计划负责人，多个用逗号隔开
    @AnTableField(column="PLAN_BEGIN_DATE", type="string,10,char", notNull=false)    private String planBeginDate;    //10.计划开始日期
    @AnTableField(column="PLAN_END_DATE", type="string,10,char", notNull=false)    private String planEndDate;    //11.计划结束日期
    @AnTableField(column="ACTUAL_BEGIN_DATE", type="string,10,char", notNull=false)    private String actualBeginDate;    //12.实际开始日期
    @AnTableField(column="ACTUAL_END_DATE", type="string,10,char", notNull=false)    private String actualEndDate;    //13.实际结束日期

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public long getPlanId()
    {
        return planId;
    }

    public void setPlanId(long planId)
    {
        this.planId = planId;
    }

    public int getPlanDate()
    {
        return planDate;
    }

    public void setPlanDate(int planDate)
    {
        this.planDate = planDate;
    }

    public int getPlanStatus()
    {
        return planStatus;
    }

    public void setPlanStatus(int planStatus)
    {
        this.planStatus = planStatus;
    }

    public int getPlanSeq()
    {
        return planSeq;
    }

    public void setPlanSeq(int planSeq)
    {
        this.planSeq = planSeq;
    }

    public int getPlanProgress()
    {
        return planProgress;
    }

    public void setPlanProgress(int planProgress)
    {
        this.planProgress = planProgress;
    }

    public Timestamp getPlanModified()
    {
        return planModified;
    }

    public void setPlanModified(Timestamp planModified)
    {
        this.planModified = planModified;
    }

    public String getPlanContent()
    {
        return planContent;
    }

    public void setPlanContent(String planContent)
    {
        this.planContent = planContent;
    }

    public String getPlanManager()
    {
        return planManager;
    }

    public void setPlanManager(String planManager)
    {
        this.planManager = planManager;
    }

    public String getPlanBeginDate()
    {
        return planBeginDate;
    }

    public void setPlanBeginDate(String planBeginDate)
    {
        this.planBeginDate = planBeginDate;
    }

    public String getPlanEndDate()
    {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate)
    {
        this.planEndDate = planEndDate;
    }

    public String getActualBeginDate()
    {
        return actualBeginDate;
    }

    public void setActualBeginDate(String actualBeginDate)
    {
        this.actualBeginDate = actualBeginDate;
    }

    public String getActualEndDate()
    {
        return actualEndDate;
    }

    public void setActualEndDate(String actualEndDate)
    {
        this.actualEndDate = actualEndDate;
    }

}
