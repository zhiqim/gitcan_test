/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.action.issue;

import org.zhiqim.gitcan.GitcanConstants;
import org.zhiqim.gitcan.dbo.GitIssue;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.kernel.paging.PageResult;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;


/**
 * 项目问题，列表&修改进度&删除
 *
 * @version v1.0.0 @author duanxiaohui 2017-10-12 新建与整理
 */
public class IssueAction implements Action, GitcanConstants
{
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        int page = request.getParameterInt("page", 1);
        int pageSize = request.getContextAttributeInt("zmr_page_size", 20);
        
        Selector selector = new org.zhiqim.orm.dbo.Selector();
        selector.addMust("projectId", GitProjectDao.getProjectId(request));
        selector.addMaybeLike("issueName", request.getParameter("issueName"));
        selector.addMaybe("issueStatus", request.getParameter("issueStatus"));
        selector.addOrderbyDesc("issueSeq");
        
        String author = request.getParameter("author");
        if (CREATOR.equals(author))
            selector.addMaybe("issueCreator", request.getSessionName());
        else if (MANAGER.equals(author))
            selector.addMaybe("issueManager", request.getSessionName());
        
        PageResult<GitIssue> result = ORM.get(ZTable.class, request).page(GitIssue.class, page, pageSize, selector);
        result.addConditionMap(request.getParameterMap());
        
        request.setAttribute("result", result); 
    }
}
