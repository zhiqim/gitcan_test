/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 机房表 对应表《GIT_IDC》
 */
@AnAlias("GitIdc")
@AnNew
@AnTable(table="GIT_IDC", key="IDC_ID", type="InnoDB")
public class GitIdc implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="IDC_ID", type="long", notNull=true)    private long idcId;    //2.机房编号
    @AnTableField(column="IDC_NAME", type="string,64", notNull=true)    private String idcName;    //3.机房名称
    @AnTableField(column="IDC_STATUS", type="byte", notNull=true)    private int idcStatus;    //4.机房状态，0：正常，1：停用
    @AnTableField(column="IDC_SEQ", type="int", notNull=true)    private int idcSeq;    //5.机房排序数，（从小大到）
    @AnTableField(column="IDC_DESC", type="string,128", notNull=true)    private String idcDesc;    //6.机房备注

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public long getIdcId()
    {
        return idcId;
    }

    public void setIdcId(long idcId)
    {
        this.idcId = idcId;
    }

    public String getIdcName()
    {
        return idcName;
    }

    public void setIdcName(String idcName)
    {
        this.idcName = idcName;
    }

    public int getIdcStatus()
    {
        return idcStatus;
    }

    public void setIdcStatus(int idcStatus)
    {
        this.idcStatus = idcStatus;
    }

    public int getIdcSeq()
    {
        return idcSeq;
    }

    public void setIdcSeq(int idcSeq)
    {
        this.idcSeq = idcSeq;
    }

    public String getIdcDesc()
    {
        return idcDesc;
    }

    public void setIdcDesc(String idcDesc)
    {
        this.idcDesc = idcDesc;
    }

}
