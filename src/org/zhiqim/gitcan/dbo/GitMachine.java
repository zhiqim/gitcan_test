/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 服务器信息表 对应表《GIT_MACHINE》
 */
@AnAlias("GitMachine")
@AnNew
@AnTable(table="GIT_MACHINE", key="MACHINE_ID", type="InnoDB")
public class GitMachine implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="IDC_ID", type="long", notNull=true)    private long idcId;    //2.机房编号
    @AnTableField(column="MACHINE_ID", type="long", notNull=true)    private long machineId;    //3.服务器编号
    @AnTableField(column="MACHINE_IP", type="string,15", notNull=true)    private String machineIp;    //4.服务器IP
    @AnTableField(column="MACHINE_NAME", type="string,64", notNull=true)    private String machineName;    //5.服务器名称
    @AnTableField(column="MACHINE_STATUS", type="byte", notNull=true)    private int machineStatus;    //6.服务器状态，0：正常，1：停用
    @AnTableField(column="MACHINE_TYPE", type="byte", notNull=true)    private int machineType;    //7.服务器类型，0：Windows，1：Linux
    @AnTableField(column="MACHINE_USER", type="string,32", notNull=true)    private String machineUser;    //8.服务器管理员账号
    @AnTableField(column="MACHINE_PASSWORD", type="string,32", notNull=true)    private String machinePassword;    //9.服务器管理员密码
    @AnTableField(column="MACHINE_PORT", type="int", notNull=true)    private int machinePort;    //10.服务器管理端口
    @AnTableField(column="MACHINE_SEQ", type="int", notNull=true)    private int machineSeq;    //11.服务器排序数，（从小大到）
    @AnTableField(column="MACHINE_INTERNET_IP", type="string,15", notNull=false)    private String machineInternetIp;    //12.服务器公网IP
    @AnTableField(column="MACHINE_EXPIRATION_DATE", type="string,10", notNull=false)    private String machineExpirationDate;    //13.服务器到期时间

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public long getIdcId()
    {
        return idcId;
    }

    public void setIdcId(long idcId)
    {
        this.idcId = idcId;
    }

    public long getMachineId()
    {
        return machineId;
    }

    public void setMachineId(long machineId)
    {
        this.machineId = machineId;
    }

    public String getMachineIp()
    {
        return machineIp;
    }

    public void setMachineIp(String machineIp)
    {
        this.machineIp = machineIp;
    }

    public String getMachineName()
    {
        return machineName;
    }

    public void setMachineName(String machineName)
    {
        this.machineName = machineName;
    }

    public int getMachineStatus()
    {
        return machineStatus;
    }

    public void setMachineStatus(int machineStatus)
    {
        this.machineStatus = machineStatus;
    }

    public int getMachineType()
    {
        return machineType;
    }

    public void setMachineType(int machineType)
    {
        this.machineType = machineType;
    }

    public String getMachineUser()
    {
        return machineUser;
    }

    public void setMachineUser(String machineUser)
    {
        this.machineUser = machineUser;
    }

    public String getMachinePassword()
    {
        return machinePassword;
    }

    public void setMachinePassword(String machinePassword)
    {
        this.machinePassword = machinePassword;
    }

    public int getMachinePort()
    {
        return machinePort;
    }

    public void setMachinePort(int machinePort)
    {
        this.machinePort = machinePort;
    }

    public int getMachineSeq()
    {
        return machineSeq;
    }

    public void setMachineSeq(int machineSeq)
    {
        this.machineSeq = machineSeq;
    }

    public String getMachineInternetIp()
    {
        return machineInternetIp;
    }

    public void setMachineInternetIp(String machineInternetIp)
    {
        this.machineInternetIp = machineInternetIp;
    }

    public String getMachineExpirationDate()
    {
        return machineExpirationDate;
    }

    public void setMachineExpirationDate(String machineExpirationDate)
    {
        this.machineExpirationDate = machineExpirationDate;
    }

}
