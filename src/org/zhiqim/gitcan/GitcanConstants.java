/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan;

/**
 * 日历常量
 *
 * @version v1.0.0 @author zouzhigang 2017-11-8 新建与整理
 */
public interface GitcanConstants
{
    //项目日历
    public static final String PREV_MONTH               = "prevMonth";
    public static final String CURR_MONTH               = "currMonth";
    public static final String NEXT_MONTH               = "nextMonth";
    
    //项目计划总结类型
    public static final int PJ_PLAN                     = 0;
    public static final int PJ_WEEK_AIM                 = 1;
    public static final int PJ_WEEK_SUM                 = 2;
    public static final int PJ_MONTH_SUM                = 3;
    
    //个人计划总结类型
    public static final int PS_PLAN                     = 0;
    public static final int PS_SUM_DAY                  = 1;
    public static final int PS_SUM_WEEK                 = 2;
    public static final int PS_SUM_MONTH                = 3;
    
    public static final int TYPE_REQUIREMENT            = 1;
    public static final int TYPE_BUG                    = 2;
    public static final int TYPE_DEFECT                 = 3;
    
    //问题进度
    public static final String CREATED                  = "created";        //新创建
    public static final String HANDLING                 = "handling";       //处理中
    public static final String HANGUP                   = "hangup";         //已挂起
    public static final String COMPLETED                = "completed";      //已解决
    public static final String CLOSED                   = "closed";         //已关闭
    public static final String REJECT                   = "reject";         //已打回
    
    public static final String CREATOR                  = "creator";
    public static final String MANAGER                  = "manager";
}
