/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.model;

import org.zhiqim.kernel.util.Strings;

/**
 * 日历星期模型，根据休息日划分出的星期模型
 *
 * @version v1.0.0 @author zouzhigang 2017-11-8 新建与整理
 */
public class CalendarWeekModel
{
    private int beginDate;
    private int beginYear;
    private int beginMonth;
    private int beginDay;
    
    private int endDate;
    private int endYear;
    private int endMonth;
    private int endDay;

    public CalendarWeekModel(int beginDate)
    {
        this.beginDate = beginDate;

        this.beginYear = beginDate / 10000;
        this.beginMonth = beginDate / 100 % 100;
        this.beginDay = beginDate % 10000;
    }
    
    public void setEndDate(int endDate)
    {
        this.endDate = endDate;
        
        this.endYear = endDate / 10000;
        this.endMonth = endDate / 100 % 100;
        this.endDay = endDate % 10000;
    }
    
    public int getBeginDate()
    {
        return beginDate;
    }

    public int getEndDate()
    {
        return endDate;
    }
    
    public int getBeginYear()
    {
        return beginYear;
    }

    public int getBeginMonth()
    {
        return beginMonth;
    }

    public int getBeginDay()
    {
        return beginDay;
    }

    public int getEndYear()
    {
        return endYear;
    }

    public int getEndMonth()
    {
        return endMonth;
    }

    public int getEndDay()
    {
        return endDay;
    }

    public String getWeekBegin()
    {
        return beginYear + "-" + Strings.prefixZero(beginMonth, 2) + "-" + Strings.prefixZero(beginDay, 2);
    }
    
    public String getWeekEnd()
    {
        return endYear + "-" + Strings.prefixZero(endMonth, 2) + "-" + Strings.prefixZero(endDay, 2);
    }
}
