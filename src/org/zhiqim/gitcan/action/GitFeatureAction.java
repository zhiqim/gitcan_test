/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.zhiqim.gitcan.dao.GitModuleDao;
import org.zhiqim.gitcan.dbo.GitFeature;
import org.zhiqim.gitcan.dbo.GitFeatureEx;
import org.zhiqim.gitcan.dbo.GitModule;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.StdSwitchAction;
import org.zhiqim.httpd.validate.ones.IsIntegerValue;
import org.zhiqim.httpd.validate.ones.IsLen;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.httpd.validate.ones.IsSelect;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.kernel.util.Sqls;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.ZView;
import org.zhiqim.orm.dbo.Selector;
import org.zhiqim.orm.dbo.Updater;

/**
 * 项目功能
 *
 * @version v1.0.0 @author huangsufang 2017-10-11 新建与整理
 */
public class GitFeatureAction extends StdSwitchAction
{
    @Override
    protected void validateId(HttpRequest request)
    {
        request.addValidate(new IsNotEmpty("featureId", "请选择一个选项"));
    }

    @Override
    protected void validateForm(HttpRequest request)
    {
        request.addValidate(new IsLen("featureName", "功能名称不能为空且不能超过32个字符", 1, 32));
        request.addValidate(new IsSelect("moduleId", "请选择所属模块，没有请先新建模块"));
        request.addValidate(new IsNotEmpty("featureStatus", "请选择功能状态"));
        request.addValidate(new IsLen("featureDesc", "功能描述不能为空且不能超过100个字符", 1, 100));
        request.addValidate(new IsIntegerValue("featureSeq", "功能排序必须是[0, 999999]范围的非负整数", 0, 999999));
    }

    @Override
    protected void list(HttpRequest request) throws Exception
    {
        long moduleId = request.getParameterLong("moduleId");
        long projectId = GitProjectDao.getProjectId(request);
        List<GitModule> moduleList = GitModuleDao.list(request, projectId);
        
        Selector selector = new Selector();
        selector.addMust("projectId", projectId);
        selector.addMaybe("moduleId", moduleId);
        selector.addOrderbyAsc("moduleId,featureSeq");
        List<GitFeatureEx> featureList = ORM.get(ZView.class, request).list(GitFeatureEx.class, selector);
        
        LinkedHashMap<Long, List<GitFeature>> moduleMap = new LinkedHashMap<>();
        
        for (GitFeature item : featureList)
        {
            List<GitFeature> list = moduleMap.get(item.getModuleId());
            if (list == null)
            {
                list = new ArrayList<>();
                moduleMap.put(item.getModuleId(), list);
            }
           
            list.add(item);
        }
        
        request.setAttribute("moduleList", moduleList);
        request.setAttribute("moduleMap", moduleMap);
    }

    @Override
    protected void add(HttpRequest request) throws Exception
    {
        request.setAttribute("moduleList", GitModuleDao.list(request));
    }

    @Override
    protected void modify(HttpRequest request) throws Exception
    {
        long featureId = request.getParameterLong("featureId");
        GitFeature item = ORM.get(ZTable.class, request).item(GitFeature.class, featureId);
        if (item == null)
        {
            request.returnHistory("功能不存在，请重新选择！");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
 
        request.setAttribute("item", item);
        request.setAttribute("moduleList", GitModuleDao.list(request));
    }

    @Override
    protected void insert(HttpRequest request) throws Exception
    {
        long projectId = GitProjectDao.getProjectId(request);
        Timestamp dateTime = Sqls.nowTimestamp();
       
        GitFeature feature = request.getParameter(GitFeature.class);
        feature.setProjectId(projectId);
        feature.setFeatureId(Ids.longId());
        feature.setFeatureCreated(dateTime);
        feature.setFeatureModified(dateTime);
        
        ORM.get(ZTable.class, request).insert(feature);
        
        GitMemberDao.report(request, projectId, request.getSessionName(), "创建了功能", feature.getFeatureName());
    }

    @Override
    protected void update(HttpRequest request) throws Exception
    {
        long featureId = request.getParameterLong("featureId");
        GitFeature item = ORM.get(ZTable.class, request).item(GitFeature.class, featureId);
        if(item == null)
        {
            request.returnHistory("功能不存在，请重新选择");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }

        Updater updater = new Updater();
        updater.addMust("featureId", featureId);
        updater.addField("moduleId", request.getParameterLong("moduleId"));
        updater.addField("featureModified", Sqls.nowTimestamp());
        updater.addField("featureSeq", request.getParameterInt("featureSeq"));
        updater.addField("featureName", request.getParameter("featureName"));
        updater.addField("featureStatus", request.getParameterInt("featureStatus"));
        updater.addField("featureDesc", request.getParameter("featureDesc"));
        
        ORM.get(ZTable.class, request).update(GitFeature.class, updater);
        
        GitMemberDao.report(request, item.getProjectId(), request.getSessionName(), "修改了功能", request.getParameter("featureName"));
    }

    @Override
    protected void delete(HttpRequest request) throws Exception
    {
        long featureId = request.getParameterLong("featureId");
        GitFeature item = ORM.get(ZTable.class, request).item(GitFeature.class, featureId);
        if(item == null)
        {
            request.returnHistory("功能不存在，请重新选择");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }

        ORM.get(ZTable.class, request).delete(GitFeature.class, featureId);
        
        GitMemberDao.report(request, item.getProjectId(), request.getSessionName(), "删除了功能", item.getFeatureName());
    }
}
