/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.presenter;

import org.zhiqim.gitcan.dbo.GitProjectPlan;
import org.zhiqim.gitcan.dbo.GitProjectSummary;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.annotation.AnIntercept;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.kernel.util.Ints;
import org.zhiqim.kernel.util.Sqls;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;
import org.zhiqim.orm.dbo.Updater;

/**
 * 项目计划总结展示器
 *
 * @version v1.0.0 @author zouzhigang 2017-11-7 新建与整理
 */
@AnAlias("GitProjectPlanPresenter")
@AnIntercept("chkZmrLogin")
public class GitProjectPlanPresenter
{
    /***********************************************************************************************/
    //项目计划增加&更新内容&更新状态&删除
    /***********************************************************************************************/
    
    /**
     * 增加项目工作计划
     * 
     * @param request       请求
     * @param date          周起始日期
     * @param status        计划状态
     * @param content       计划内容
     * @throws Exception    异常
     */
    public static void doAddProjectPlan(HttpRequest request, long id, int date, String type, String value) throws Exception
    {
        if (id == 0)
            id = Ids.longId();
        
        GitProjectPlan plan = new GitProjectPlan();
        if ("planStatus".equals(type))
            plan.setPlanStatus(Ints.toInt(value));
        else if ("planBeginDate".equals(type))
            plan.setPlanBeginDate(value);
        else if ("planEndDate".equals(type))
            plan.setPlanEndDate(value);
        else if ("actualBeginDate".equals(type))
            plan.setActualBeginDate(value);
        else if ("actualEndDate".equals(type))
            plan.setActualEndDate(value);
        else if ("planManager".equals(type))
            plan.setPlanManager(value);
        else if ("planProgress".equals(type))
            plan.setPlanProgress(Ints.toInt(value));
        else if ("planContent".equals(type))
            plan.setPlanContent(value);
        else
        {
            request.setResponseError("类型不正确");
            return;
        }
        
        plan.setProjectId(GitProjectDao.getProjectId(request));
        plan.setPlanId(id);
        plan.setPlanDate(date);
        plan.setPlanModified(Sqls.nowTimestamp());
        
        //1.先取出最大的sequence
        Selector selector = new Selector("planSeq");
        selector.addMust("projectId", GitProjectDao.getProjectId(request));
        selector.addMust("planDate", date);
        selector.addOrderbyDesc("planSeq");
        GitProjectPlan item = ORM.get(ZTable.class, request).item(GitProjectPlan.class, selector);
        
        //2.+1设置到新的sequence
        plan.setPlanSeq((item == null)?1:item.getPlanSeq()+1);
        
        ORM.get(ZTable.class, request).insert(plan);
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "增加了计划", plan.getPlanContent());
    }
    
    /**
     * 更新项目工作计划
     * 
     * @param request       请求
     * @param id            计划编号
     * @param date          周起始日期
     * @param content       计划内容
     * @throws Exception    异常
     */
    public static void doUpdateProjectPlan(HttpRequest request, long id, int date, String type, String value) throws Exception
    {
        GitProjectPlan plan = ORM.get(ZTable.class, request).item(GitProjectPlan.class, id);
        if (plan == null)
        {//新增，默认状态正常
            doAddProjectPlan(request, id, date, type, value);
            return;
        }
        
        //修改指定值
        Updater updater = new Updater();
        if ("planStatus".equals(type))
            updater.addField("planStatus", Ints.toInt(value));
        else if ("planBeginDate".equals(type))
            updater.addField("planBeginDate", value);
        else if ("planEndDate".equals(type))
            updater.addField("planEndDate", value);
        else if ("actualBeginDate".equals(type))
            updater.addField("actualBeginDate", value);
        else if ("actualEndDate".equals(type))
            updater.addField("actualEndDate", value);
        else if ("planManager".equals(type))
            updater.addField("planManager", value);
        else if ("planProgress".equals(type))
            updater.addField("planProgress", Ints.toInt(value));
        else if ("planContent".equals(type))
            updater.addField("planContent", value);
        else
        {
            request.setResponseError("类型不正确");
            return;
        }
        
        updater.addMust("planId", id);
        updater.addField("planModified", Sqls.nowTimestamp());
        
        ORM.get(ZTable.class, request).update(GitProjectPlan.class, updater);
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "修改了计划", plan.getPlanContent());
    }
    
    /**
     * 更新项目工作总结
     * 
     * @param request       请求
     * @param id            计划编号
     * @param date          周起始日期
     * @param status        计划状态
     * @throws Exception    异常
     */
    public static void doUpdateProjectPlanStatus(HttpRequest request, long id, int date, int status) throws Exception
    {
        GitProjectPlan plan = ORM.get(ZTable.class, request).item(GitProjectPlan.class, id);
        if (plan == null)
        {//新增，默认内容为null
            doAddProjectPlan(request, id, date, "planStatus", ""+status);
            return;
        }
        
        //修改状态
        Updater updater = new Updater();
        updater.addMust("planId", id);
        updater.addField("planModified", Sqls.nowTimestamp());
        updater.addField("planStatus", status);
        
        ORM.get(ZTable.class, request).update(GitProjectPlan.class, updater);
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "修改了计划状态", plan.getPlanContent());
    }
    
    /**
     * 删除项目工作计划
     * 
     * @param request       请求
     * @param id            计划编号
     * @throws Exception    异常
     */
    public static void doDeleteProjectPlan(HttpRequest request, long id) throws Exception
    {
        GitProjectPlan plan = ORM.get(ZTable.class, request).item(GitProjectPlan.class, id);
        
        ORM.get(ZTable.class, request).delete(GitProjectPlan.class, id);
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "删除了项目计划", plan.getPlanContent());
    }
    
    /***********************************************************************************************/
    //项目总结更新（无则插入，有则更新）
    /***********************************************************************************************/
    
    
    /**
     * 更新项目工作总结
     * 
     * @param request       请求
     * @param type          总结类型
     * @param date          总结日期
     * @param content       总结内容
     * @throws Exception    异常
     */
    public static void doUpdateProjectSummary(HttpRequest request, int type, int date, String content) throws Exception
    {
        Selector selector = new Selector("summaryId");
        selector.addMust("projectId", GitProjectDao.getProjectId(request));
        selector.addMust("summaryType", type);
        selector.addMust("summaryDate", date);
        GitProjectSummary item = ORM.get(ZTable.class, request).item(GitProjectSummary.class, selector);
        
        if (item == null)
        {//不存在则增加
            item = new GitProjectSummary();
            item.setProjectId(GitProjectDao.getProjectId(request));
            item.setSummaryId(Ids.longId());
            item.setSummaryType(type);
            item.setSummaryDate(date);
            item.setSummaryModified(Sqls.nowTimestamp());
            item.setSummaryContent(content);
            
            ORM.get(ZTable.class, request).insert(item);
            
            GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "增加了总结", item.getSummaryContent());
        }
        else
        {
            Updater updater = new Updater();
            updater.addMust("summaryId", item.getSummaryId());
            updater.addField("summaryModified", Sqls.nowTimestamp());
            updater.addField("summaryContent", content);
            
            ORM.get(ZTable.class, request).update(GitProjectSummary.class, updater);
            
            GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "修改了总结", item.getSummaryContent());
        }
    }
}
