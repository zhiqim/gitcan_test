/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.presenter;

import java.util.List;

import org.zhiqim.gitcan.dbo.GitPersonPlan;
import org.zhiqim.gitcan.dbo.GitPersonSummary;
import org.zhiqim.gitsolo.dbo.GitReader;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.annotation.AnIntercept;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.kernel.util.Lists;
import org.zhiqim.kernel.util.Sqls;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;
import org.zhiqim.orm.dbo.Updater;

/**
 * 个人计划总结展示器
 *
 * @version v1.0.0 @author zouzhigang 2017-11-7 新建与整理
 */
@AnAlias("GitPersonPresenter")
@AnIntercept("chkZmrLogin")
public class GitPersonPresenter
{
    /***********************************************************************************************/
    //汇报增加&删除
    /***********************************************************************************************/
    
    /**
     * 批量增加汇报对象
     * 
     * @param request       请求对象
     * @param readerCodes   操作员编码组
     * @throws Exception    异常
     */
    public static void doAddReaders(HttpRequest request, String readerCodes) throws Exception
    {
        List<String> readerList = Lists.toStringList(readerCodes);
        for (String item : readerList)
        {
            GitReader reader = new GitReader();
            reader.setOperatorCode(request.getSessionName());
            reader.setReaderCode(item);
            
            ORM.get(ZTable.class, request).replace(reader);
        }
    }
    
    /**
     * 删除汇报对象
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @param readerCode    汇报对象编码
     * @throws Exception    异常
     */
    public static void doDeleteReader(HttpRequest request, String readerCode) throws Exception
    {
        Selector selector = new Selector();
        selector.addMust("operatorCode", request.getSessionName());
        selector.addMust("readerCode", readerCode);
        
        ORM.get(ZTable.class, request).delete(GitReader.class, selector);
    }
    
    /**
     * 删除向我汇报的操作员
     * 
     * @param request       请求对象
     * @param projectId     项目编号
     * @param operatorCode  操作员编码
     * @throws Exception    异常
     */
    public static void doDeleteOperator(HttpRequest request, String operatorCode) throws Exception
    {
        Selector selector = new Selector();
        selector.addMust("readerCode", request.getSessionName());
        selector.addMust("operatorCode", operatorCode);
        
        ORM.get(ZTable.class, request).delete(GitReader.class, selector);
    }
    
    /***********************************************************************************************/
    //个人计划增加&更新内容&更新状态&删除
    /***********************************************************************************************/
    
    /**
     * 增加个人工作计划
     * 
     * @param request       请求
     * @param date          周起始日期
     * @param status        计划状态
     * @param content       计划内容
     * @throws Exception    异常
     */
    public static void doAddPersonPlan(HttpRequest request, int date, int status, String content) throws Exception
    {
        //1.先取出最大的sequence
        Selector selector = new Selector("planSeq");
        selector.addMust("operatorCode", request.getSessionName());
        selector.addMust("planDate", date);
        selector.addOrderbyDesc("planSeq");
        GitPersonPlan item = ORM.get(ZTable.class, request).item(GitPersonPlan.class, selector);
        
        //2.插入到数据库
        GitPersonPlan plan = new GitPersonPlan();
        plan.setOperatorCode(request.getSessionName());
        plan.setPlanId(Ids.longId());
        plan.setPlanDate(date);
        plan.setPlanStatus(status);
        plan.setPlanSeq((item == null)?1:item.getPlanSeq()+1);
        plan.setPlanContent(content);
        plan.setPlanModified(Sqls.nowTimestamp());
        
        ORM.get(ZTable.class, request).insert(plan);
    }
    
    /**
     * 更新个人工作计划
     * 
     * @param request       请求
     * @param id            计划编号
     * @param date          周起始日期
     * @param content       计划内容
     * @throws Exception    异常
     */
    public static void doUpdatePersonPlan(HttpRequest request, long id, int date, String content) throws Exception
    {
        if (id == 0)
        {//新增，默认状态正常
            doAddPersonPlan(request, date, 0, content);
            return;
        }
        
        //修改
        Updater updater = new Updater();
        updater.addMust("planId", id);
        updater.addField("planModified", Sqls.nowTimestamp());
        updater.addField("planContent", content);
        
        ORM.get(ZTable.class, request).update(GitPersonPlan.class, updater);
    }
    
    /**
     * 更新个人工作总结
     * 
     * @param request       请求
     * @param id            计划编号
     * @param date          周起始日期
     * @param status        计划状态
     * @throws Exception    异常
     */
    public static void doUpdatePersonPlanStatus(HttpRequest request, long id, int date, int status) throws Exception
    {
        if (id == 0)
        {//新增，默认内容为null
            doAddPersonPlan(request, date, status, null);
            return;
        }
        
        //修改
        Updater updater = new Updater();
        updater.addMust("planId", id);
        updater.addField("planModified", Sqls.nowTimestamp());
        updater.addField("planStatus", status);
        
        ORM.get(ZTable.class, request).update(GitPersonPlan.class, updater);
    }
    
    /**
     * 删除个人工作计划
     * 
     * @param request       请求
     * @param id            计划编号
     * @throws Exception    异常
     */
    public static void doDeletePersonPlan(HttpRequest request, long id) throws Exception
    {
        ORM.get(ZTable.class, request).delete(GitPersonPlan.class, id);
    }
    
    /***********************************************************************************************/
    //个人总结更新（无则插入，有则更新）
    /***********************************************************************************************/
    
    
    /**
     * 更新个人工作总结
     * 
     * @param request       请求
     * @param type          总结类型
     * @param date          总结日期
     * @param content       总结内容
     * @throws Exception    异常
     */
    public static void doUpdatePersonSummary(HttpRequest request, int type, int date, String content) throws Exception
    {
        Selector selector = new Selector("summaryId");
        selector.addMust("operatorCode", request.getSessionName());
        selector.addMust("summaryType", type);
        selector.addMust("summaryDate", date);
        GitPersonSummary item = ORM.get(ZTable.class, request).item(GitPersonSummary.class, selector);
        
        if (item == null)
        {//不存在则增加
            item = new GitPersonSummary();
            item.setOperatorCode(request.getSessionName());
            item.setSummaryId(Ids.longId());
            item.setSummaryType(type);
            item.setSummaryDate(date);
            item.setSummaryModified(Sqls.nowTimestamp());
            item.setSummaryContent(content);
            
            ORM.get(ZTable.class, request).insert(item);
        }
        else
        {
            Updater updater = new Updater();
            updater.addMust("summaryId", item.getSummaryId());
            updater.addField("summaryModified", Sqls.nowTimestamp());
            updater.addField("summaryContent", content);
            
            ORM.get(ZTable.class, request).update(GitPersonSummary.class, updater);
        }
    }
}
