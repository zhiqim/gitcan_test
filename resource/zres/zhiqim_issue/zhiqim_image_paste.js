/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[欢迎加盟知启蒙，一起邂逅框架梦]

 * 
 * https://www.zhiqim.com/gitcan/zhiqim/zhiqim_issue.htm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
+(function(Z)
{
//BEGIN

Z.ImagePaste = Z.Class.newInstance();
Z.ImagePaste.prototype = 
{
    defaults:
    {//传入数据存放元素和显示元素对象或编号
        dataElem: null,
        showElem: null
    },
    
    execute: function()
    {
        this.$dataElem = Z.$elem(this.dataElem, "Z.ImagePaste", "dataElem");
        this.$showElem = Z.$elem(this.showElem, "Z.ImagePaste", "showElem");
        
        //注册粘贴事件
        Z(window).on("paste", this.onPaste, this);
    },
    
    onPaste: function(e)
    {//粘贴
        var data = e.clipboardData;
        if(!data || !data.items || !data.types)
        {//无数据结束
            return;
        }
        
        //找到一个文件类型，并且是图片格式的
        var theItem;
        for(var i=0;i<data.types.length;i++)
        {
            if(data.types[i] !== "Files")
                continue;
            
            var item = data.items[i];
            if (!item || item.kind != "file" || !(/^image\//i.test(item.type)))
                continue;
            
            theItem = item;
        }

        if (!theItem)
        {//未找到结束
            return;
        }
        
        var reader = new FileReader();
        reader.onload = Z.bind(this.onRead, this);
        reader.readAsDataURL(theItem.getAsFile());
    },
    
    onRead: function(e)
    {
        var result = e.target.result;
        if (Z.EL.has(this.$dataElem[0], "value"))
            this.$dataElem.val(result);
        else
            this.$dataElem.text(result);
        
        this.$showElem.html("<img src="+result+">");
    }
}

//END
})(zhiqim);