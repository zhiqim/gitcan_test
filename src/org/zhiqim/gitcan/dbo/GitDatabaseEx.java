/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import org.zhiqim.gitcan.dbo.GitDatabase;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 数据库扩展视图 对应视图《GIT_DATABASE_EX》
 */
@AnAlias("GitDatabaseEx")
@AnNew
@AnView("GIT_DATABASE,GIT_IDC")
@AnViewJoin({@AnViewJoinValue(type="EQUAL", lTable="GIT_DATABASE", lColumn="PROJECT_ID", rTable="GIT_IDC", rColumn="PROJECT_ID"),
             @AnViewJoinValue(type="EQUAL", lTable="GIT_DATABASE", lColumn="IDC_ID", rTable="GIT_IDC", rColumn="IDC_ID")})
public class GitDatabaseEx extends GitDatabase
{
    private static final long serialVersionUID = 1L;

    @AnViewField(table="GIT_IDC", column="IDC_NAME")    private String idcName;    //2.机房名称

    public String toString()
    {
        return Jsons.toString(this);
    }

    public String getIdcName()
    {
        return idcName;
    }

    public void setIdcName(String idcName)
    {
        this.idcName = idcName;
    }

}
