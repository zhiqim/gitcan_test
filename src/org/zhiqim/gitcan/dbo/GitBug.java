/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目bug表 对应表《GIT_BUG》
 */
@AnAlias("GitBug")
@AnNew
@AnTable(table="GIT_BUG", key="ISSUE_ID", type="InnoDB")
public class GitBug implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="ISSUE_ID", type="long", notNull=true)    private long issueId;    //1.问题编号
    @AnTableField(column="MODULE_ID", type="long", notNull=true)    private long moduleId;    //2.模块编号
    @AnTableField(column="BUG_LEVEL", type="byte", notNull=true)    private int bugLevel;    //3.BUG严重程度，3：致命，2：严重，1：轻微
    @AnTableField(column="BUG_IMAGE", type="binary", notNull=false)    private byte[] bugImage;    //4.BUG图片

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getIssueId()
    {
        return issueId;
    }

    public void setIssueId(long issueId)
    {
        this.issueId = issueId;
    }

    public long getModuleId()
    {
        return moduleId;
    }

    public void setModuleId(long moduleId)
    {
        this.moduleId = moduleId;
    }

    public int getBugLevel()
    {
        return bugLevel;
    }

    public void setBugLevel(int bugLevel)
    {
        this.bugLevel = bugLevel;
    }

    public byte[] getBugImage()
    {
        return bugImage;
    }

    public void setBugImage(byte[] bugImage)
    {
        this.bugImage = bugImage;
    }

}
