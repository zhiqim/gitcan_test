/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.action.equipment;

import java.util.List;

import org.zhiqim.gitcan.dbo.GitIdc;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.StdSwitchAction;
import org.zhiqim.httpd.validate.ones.IsIntegerValue;
import org.zhiqim.httpd.validate.ones.IsLen;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;
import org.zhiqim.orm.dbo.Updater;


/**
 * 机房管理，支持机房的创建、删除和修改权限
 *
 * @version v1.0.0 @author zhichenggang 2014-3-21 新建与整理
 */
public class IdcAction extends StdSwitchAction
{
    @Override
    protected void validateId(HttpRequest request)
    {
        request.addValidate(new IsNotEmpty("idcId", "请选择一个选项"));
    }

    @Override
    protected void validateForm(HttpRequest request)
    {
        request.addValidate(new IsLen("idcName", "机房名称不能为空，[1, 32]范围", 1, 32));
        request.addValidate(new IsIntegerValue("idcStatus", "机房状态仅支持[正常|停用]", 0, 1));
        request.addValidate(new IsIntegerValue("idcSeq", "机房排序必须是[0, 999999]范围的非负整数", 0, 999999));
    }

    @Override
    protected void list(HttpRequest request) throws Exception
    {
        Selector selector = new Selector();
        selector.addMust("projectId", GitProjectDao.getProjectId(request));
        selector.addOrderbyAsc("idcSeq");
        
        List<GitIdc> list = ORM.get(ZTable.class, request).list(GitIdc.class, selector);
        request.setAttribute("list", list);
    }
    
    @Override
    protected void add(HttpRequest request) throws Exception
    {
    }

    @Override
    protected void modify(HttpRequest request) throws Exception
    {
        long idcId = request.getParameterLong("idcId");
        GitIdc item = ORM.get(ZTable.class, request).item(GitIdc.class, idcId);
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        request.setAttribute("item", item);
    }

    @Override
    protected void insert(HttpRequest request) throws Exception
    {
        String idcName = request.getParameter("idcName");
        int idcStatus = request.getParameterInt("idcStatus");
        int idcSeq = request.getParameterInt("idcSeq");
        String idcDesc = request.getParameter("idcDesc");
        
        GitIdc item = new GitIdc();
        item.setProjectId(GitProjectDao.getProjectId(request));
        item.setIdcId(Ids.longId());
        item.setIdcName(idcName);
        item.setIdcStatus(idcStatus);
        item.setIdcSeq(idcSeq);
        item.setIdcDesc(idcDesc);
        
        ORM.get(ZTable.class, request).insert(item);
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "创建了机房", idcName);
    }

    @Override
    protected void update(HttpRequest request) throws Exception
    {
        long idcId = request.getParameterLong("idcId");
        GitIdc item = ORM.get(ZTable.class, request).item(GitIdc.class, idcId);
        if(item == null)
        {
            request.returnHistory("机房不存在，请重新选择");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        Updater updater = new Updater();
        updater.addMust("idcId", idcId);
        updater.addField("idcName", request.getParameter("idcName"));
        updater.addField("idcStatus", request.getParameterInt("idcStatus"));
        updater.addField("idcSeq", request.getParameterInt("idcSeq"));
        updater.addField("idcDesc", request.getParameter("idcDesc"));
        
        ORM.get(ZTable.class, request).update(GitIdc.class, updater);
        
        GitMemberDao.report(request, item.getProjectId(), request.getSessionName(), "修改了机房", request.getParameter("idcName"));
    }

    @Override
    protected void delete(HttpRequest request) throws Exception
    {
        long idcId = request.getParameterLong("idcId");
        GitIdc item = ORM.get(ZTable.class, request).item(GitIdc.class, idcId);
        if(item == null)
        {
            request.returnHistory("机房不存在，请重新选择");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        ORM.get(ZTable.class, request).delete(GitIdc.class, idcId);
        
        GitMemberDao.report(request, item.getProjectId(), request.getSessionName(), "删除了机房", item.getIdcName());
    }
}
