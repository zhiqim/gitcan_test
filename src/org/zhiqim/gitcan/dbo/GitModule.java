/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;
import java.sql.Timestamp;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目模块表 对应表《GIT_MODULE》
 */
@AnAlias("GitModule")
@AnNew
@AnTable(table="GIT_MODULE", key="MODULE_ID", type="InnoDB")
public class GitModule implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="MODULE_ID", type="long", notNull=true)    private long moduleId;    //2.模块编号
    @AnTableField(column="MODULE_NAME", type="string,32", notNull=true)    private String moduleName;    //3.模块名称
    @AnTableField(column="MODULE_SEQ", type="int", notNull=true)    private int moduleSeq;    //4.模块排序数
    @AnTableField(column="MODULE_CREATED", type="datetime", notNull=true)    private Timestamp moduleCreated;    //5.创建时间
    @AnTableField(column="MODULE_MODIFIED", type="datetime", notNull=true)    private Timestamp moduleModified;    //6.更新时间
    @AnTableField(column="MODULE_DESC", type="string,128", notNull=false)    private String moduleDesc;    //7.模块描述

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public long getModuleId()
    {
        return moduleId;
    }

    public void setModuleId(long moduleId)
    {
        this.moduleId = moduleId;
    }

    public String getModuleName()
    {
        return moduleName;
    }

    public void setModuleName(String moduleName)
    {
        this.moduleName = moduleName;
    }

    public int getModuleSeq()
    {
        return moduleSeq;
    }

    public void setModuleSeq(int moduleSeq)
    {
        this.moduleSeq = moduleSeq;
    }

    public Timestamp getModuleCreated()
    {
        return moduleCreated;
    }

    public void setModuleCreated(Timestamp moduleCreated)
    {
        this.moduleCreated = moduleCreated;
    }

    public Timestamp getModuleModified()
    {
        return moduleModified;
    }

    public void setModuleModified(Timestamp moduleModified)
    {
        this.moduleModified = moduleModified;
    }

    public String getModuleDesc()
    {
        return moduleDesc;
    }

    public void setModuleDesc(String moduleDesc)
    {
        this.moduleDesc = moduleDesc;
    }

}
