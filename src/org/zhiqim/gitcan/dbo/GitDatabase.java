/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 数据库部署表 对应表《GIT_DATABASE》
 */
@AnAlias("GitDatabase")
@AnNew
@AnTable(table="GIT_DATABASE", key="DATABASE_ID", type="InnoDB")
public class GitDatabase implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="IDC_ID", type="long", notNull=true)    private long idcId;    //2.机房编号
    @AnTableField(column="DATABASE_ID", type="long", notNull=true)    private long databaseId;    //3.数据库编号
    @AnTableField(column="DATABASE_NAME", type="string,64", notNull=true)    private String databaseName;    //4.数据库名称
    @AnTableField(column="DATABASE_TYPE", type="byte", notNull=true)    private int databaseType;    //5.数据库类型，目前支持4种，0：MySQL，1：Oracle，2：MSSQL，3：PostpreSQL
    @AnTableField(column="DATABASE_STATUS", type="byte", notNull=true)    private int databaseStatus;    //6.数据库状态
    @AnTableField(column="DATABASE_SEQ", type="int", notNull=true)    private int databaseSeq;    //7.数据库排序数，（从小大到）
    @AnTableField(column="DATABASE_IP", type="string,50", notNull=true)    private String databaseIp;    //8.数据库IP或域名
    @AnTableField(column="DATABASE_PORT", type="int", notNull=true)    private int databasePort;    //9.数据库监听端口
    @AnTableField(column="DATABASE_USER", type="string,32", notNull=true)    private String databaseUser;    //10.数据库管理员账号
    @AnTableField(column="DATABASE_PASSWORD", type="string,32", notNull=true)    private String databasePassword;    //11.数据库管理员密码
    @AnTableField(column="DATABASE_EXPIRATION_DATE", type="string,10", notNull=false)    private String databaseExpirationDate;    //12.数据库到期时间

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public long getIdcId()
    {
        return idcId;
    }

    public void setIdcId(long idcId)
    {
        this.idcId = idcId;
    }

    public long getDatabaseId()
    {
        return databaseId;
    }

    public void setDatabaseId(long databaseId)
    {
        this.databaseId = databaseId;
    }

    public String getDatabaseName()
    {
        return databaseName;
    }

    public void setDatabaseName(String databaseName)
    {
        this.databaseName = databaseName;
    }

    public int getDatabaseType()
    {
        return databaseType;
    }

    public void setDatabaseType(int databaseType)
    {
        this.databaseType = databaseType;
    }

    public int getDatabaseStatus()
    {
        return databaseStatus;
    }

    public void setDatabaseStatus(int databaseStatus)
    {
        this.databaseStatus = databaseStatus;
    }

    public int getDatabaseSeq()
    {
        return databaseSeq;
    }

    public void setDatabaseSeq(int databaseSeq)
    {
        this.databaseSeq = databaseSeq;
    }

    public String getDatabaseIp()
    {
        return databaseIp;
    }

    public void setDatabaseIp(String databaseIp)
    {
        this.databaseIp = databaseIp;
    }

    public int getDatabasePort()
    {
        return databasePort;
    }

    public void setDatabasePort(int databasePort)
    {
        this.databasePort = databasePort;
    }

    public String getDatabaseUser()
    {
        return databaseUser;
    }

    public void setDatabaseUser(String databaseUser)
    {
        this.databaseUser = databaseUser;
    }

    public String getDatabasePassword()
    {
        return databasePassword;
    }

    public void setDatabasePassword(String databasePassword)
    {
        this.databasePassword = databasePassword;
    }

    public String getDatabaseExpirationDate()
    {
        return databaseExpirationDate;
    }

    public void setDatabaseExpirationDate(String databaseExpirationDate)
    {
        this.databaseExpirationDate = databaseExpirationDate;
    }

}
