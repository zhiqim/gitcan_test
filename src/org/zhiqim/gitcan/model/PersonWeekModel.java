/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.model;

import java.util.ArrayList;
import java.util.List;


/**
 * 个人计划总结一周模型
 * 1、日历星期KEY
 * 2、计划列表
 * 3、日总结列表
 * 4、周总结
 *
 * @version v1.0.0 @author zouzhigang 2017-11-8 新建与整理
 */
public class PersonWeekModel
{
    private CalendarWeekModel key;
    
    private List<PersonPlanModel> planList;
    private List<PersonDayModel> dayList;
    private PersonModel week;
    
    public PersonWeekModel(CalendarWeekModel key)
    {
        this.key = key;
        this.planList = new ArrayList<>();
        this.dayList = new ArrayList<>();
    }

    public void addPlanModel(PersonPlanModel model)
    {
        planList.add(model);
    }
    
    public void addDayModel(PersonDayModel model)
    {
        dayList.add(model);
    }
    
    public CalendarWeekModel getKey()
    {
        return key;
    }

    public PersonModel getWeek()
    {
        return week;
    }

    public void setWeek(PersonModel week)
    {
        this.week = week;
    }

    public List<PersonPlanModel> getPlanList()
    {
        return planList;
    }

    public List<PersonDayModel> getDayList()
    {
        return dayList;
    }
    
    public int getRowSize()
    {
        return planList.size() + dayList.size() + 1;
    }
    
    public String getWeekBegin()
    {
        return key.getWeekBegin();
    }
    
    public String getWeekEnd()
    {
        return key.getWeekEnd();
    }
    
    public PersonPlanModel getFirstPlan()
    {
        return planList.get(0);
    }
}
