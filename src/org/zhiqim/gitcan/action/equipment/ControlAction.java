/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.action.equipment;

import java.util.List;

import org.zhiqim.gitcan.dbo.GitControl;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.StdSwitchAction;
import org.zhiqim.httpd.validate.ones.IsIntegerValue;
import org.zhiqim.httpd.validate.ones.IsLen;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.kernel.util.Validates;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;
import org.zhiqim.orm.dbo.Updater;


/**
 * 远程控制管理，支持远程信息的创建、删除和修改权限
 *
 * @version v1.0.0 @author duanxiaohui 2017-10-25 新建与整理
 */
public class ControlAction extends StdSwitchAction
{

    @Override
    protected void validateId(HttpRequest request)
    {
        request.addValidate(new IsNotEmpty("controlId", "请选择一个选项"));
    }

    @Override
    protected void validateForm(HttpRequest request)
    {
        request.addValidate(new IsLen("controlDomain", "控制域名或IP不能为空，[1, 128]范围", 1, 32));
        request.addValidate(new IsIntegerValue("controlPort", "控制端口不能为空，[1, 65534]范围", 1, 65534));
        request.addValidate(new IsNotEmpty("controlUser", "控制管理员账户不能为空"));
        request.addValidate(new IsNotEmpty("controlName", "远程控制名称不能为空"));
    }

    @Override
    protected void list(HttpRequest request) throws Exception
    {
        Selector selector = new Selector("projectId", GitProjectDao.getProjectId(request));
        List<GitControl> list = ORM.get(ZTable.class, request).list(GitControl.class, selector);
        request.setAttribute("list", list);
    }

    @Override
    protected void add(HttpRequest request) throws Exception
    {
    }

    @Override
    protected void modify(HttpRequest request) throws Exception
    {
        long controlId = request.getParameterLong("controlId");
        GitControl item = ORM.get(ZTable.class, request).item(GitControl.class, controlId);
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        request.setAttribute("item", item);
    }

    @Override
    protected void insert(HttpRequest request) throws Exception
    {
        String controlPassword = request.getParameter("controlPassword");
        if (Validates.isEmptyBlank(controlPassword))
        {
            request.returnHistory("控制管理员密码不能为空");
            return;
        }
        
        long projectId = GitProjectDao.getProjectId(request);
        String controlDomain = request.getParameter("controlDomain");
        if (ORM.get(ZTable.class, request).count(GitControl.class, new Selector("projectId", projectId).addMust("controlDomain", controlDomain)) > 0)
        {
            request.returnHistory("该控制域名已存在，请不要重复增加");
            return;
        }
        
        String controlUser = request.getParameter("controlUser");
        int controlPort = request.getParameterInt("controlPort");
        String controlName = request.getParameter("controlName");
        
        GitControl item = new GitControl();
        item.setProjectId(projectId);
        item.setControlId(Ids.longId());
        item.setControlUser(controlUser);
        item.setControlPort(controlPort);
        item.setControlDomain(controlDomain);
        item.setControlName(controlName);
        item.setControlPassword(controlPassword);
        
        ORM.get(ZTable.class, request).insert(item);
        
        GitMemberDao.report(request, projectId, request.getSessionName(), "创建了远程控制", controlName);
    }

    @Override
    protected void update(HttpRequest request) throws Exception
    {
        long controlId = request.getParameterLong("controlId");
        GitControl item = ORM.get(ZTable.class, request).item(GitControl.class, controlId);
        if(item == null)
        {
            request.returnHistory("远程控制不存在，请重新选择");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        Updater updater = new Updater();
        updater.addMust("controlId", controlId);
        updater.addField("controlUser", request.getParameter("controlUser"));
        updater.addField("controlDomain", request.getParameter("controlDomain"));
        updater.addField("controlPort", request.getParameterInt("controlPort"));
        updater.addField("controlName", request.getParameter("controlName"));
        
        String controlPassword = request.getParameter("controlPassword");
        if (Validates.isNotEmpty(controlPassword))
        {//密码不为空才修改
            updater.addField("controlPassword", controlPassword);
        }
        
        ORM.get(ZTable.class, request).update(GitControl.class, updater);
        
        GitMemberDao.report(request, item.getProjectId(), request.getSessionName(), "修改了远程控制", request.getParameter("controlName"));
    }

    @Override
    protected void delete(HttpRequest request) throws Exception
    {
        long controlId = request.getParameterLong("controlId");
        GitControl item = ORM.get(ZTable.class, request).item(GitControl.class, controlId);
        if(item == null)
        {
            request.returnHistory("远程控制不存在，请重新选择");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        ORM.get(ZTable.class, request).delete(GitControl.class, controlId);
        
        GitMemberDao.report(request, item.getProjectId(), request.getSessionName(), "删除了远程控制", item.getControlName());
    }

}
