/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.presenter;

import org.zhiqim.gitcan.dbo.GitCalendar;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.annotation.AnIntercept;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.util.DateTimes;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;

/**
 * 日历页面调用器
 *
 * @version v1.0.0 @author zouzhigang 2017-11-8 新建与整理
 */
@AnAlias("GitCalendarPresenter")
@AnIntercept("chkZmrLogin")
public class GitCalendarPresenter 
{
    /**
     * 切换工作日和休息日
     * 
     * @param request       请求对象
     * @param calendarDate  日历日期
     * @throws Exception    异常
     */
    public static void doToggle(HttpRequest request, int calendarDate) throws Exception
    {   
       if((ORM.get(ZTable.class, request).count(GitCalendar.class, calendarDate)) > 0)
       {//判断该日期在项目日历表中是否存在，存在就删除
           ORM.get(ZTable.class, request).delete(GitCalendar.class, calendarDate);
           return;
       }

       //不存在则增加，周末就设置为工作日，否则设置为休息日
       GitCalendar calendar = new GitCalendar();
       calendar.setCalendarDate(calendarDate);
       int week = DateTimes.getDateWeek7(calendarDate);
       if (week == 6 || week == 7)
           calendar.setCalendarRest(false);
       else
           calendar.setCalendarRest(true);
       
       ORM.get(ZTable.class, request).insert(calendar);
    }
}
