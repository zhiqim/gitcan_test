/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.model;

import java.util.ArrayList;
import java.util.List;

import org.zhiqim.kernel.util.Strings;

/**
 * 项目计划总结一周模型
 * 1、日历星期KEY
 * 2、计划列表
 * 3、周目标
 * 4、周总结
 *
 * @version v1.0.0 @author zouzhigang 2017-11-8 新建与整理
 */
public class ProjectWeekModel
{
    private CalendarWeekModel key;
    
    private List<ProjectPlanModel> planList;
    private ProjectModel weekAim;
    private ProjectModel weekSummary;
    
    public ProjectWeekModel(CalendarWeekModel key)
    {
        this.key = key;
        this.planList = new ArrayList<>();
    }

    public void addPlanModel(ProjectPlanModel model)
    {
        planList.add(model);
    }
    
    public CalendarWeekModel getKey()
    {
        return key;
    }

    public ProjectModel getWeekSummary()
    {
        return weekSummary;
    }

    public void setWeekSummary(ProjectModel weekSummary)
    {
        this.weekSummary = weekSummary;
    }
    
    public ProjectModel getWeekAim()
    {
        return weekAim;
    }

    public void setWeekAim(ProjectModel weekAim)
    {
        this.weekAim = weekAim;
    }

    public List<ProjectPlanModel> getPlanList()
    {
        return planList;
    }

    public int getRowSize()
    {
        return planList.size() + 2;
    }
    
    public String getWeekName()
    {
        int beginMonth = key.getBeginMonth();
        int beginDay = key.getBeginDay();
        int endMonth = key.getEndMonth();
        int endDay = key.getEndDay();
        
        return Strings.prefixZero(beginMonth, 2) + "." + Strings.prefixZero(beginDay, 2) + "-" +
                Strings.prefixZero(endMonth, 2) + "." + Strings.prefixZero(endDay, 2);
    }
    
    public String getWeekBegin()
    {
        return key.getWeekBegin();
    }
    
    public String getWeekEnd()
    {
        return key.getWeekEnd();
    }
    
    public ProjectPlanModel getFirstPlan()
    {
        return planList.get(0);
    }
}
