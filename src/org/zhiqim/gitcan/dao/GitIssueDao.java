/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dao;

import org.zhiqim.gitcan.GitcanConstants;
import org.zhiqim.gitcan.dbo.GitBugEx;
import org.zhiqim.gitcan.dbo.GitIssue;
import org.zhiqim.kernel.annotation.AnAlias;

/**
 * 项目问题数据访问对象
 *
 * @version v1.0.0 @author zouzhigang 2017-11-9 新建与整理
 */
@AnAlias("GitIssueDao")
public class GitIssueDao implements GitcanConstants
{
    /**
     * 获取问题状态中文描述
     * 
     * @param item  问题对象
     * @return      中文描述
     */
    public static String getIssueStatusDesc(GitIssue item)
    {
        String status = item.getIssueStatus();
        if (CREATED.equals(status))
            return "<span class=\"z-color-333\">新创建</span>";
        else if (HANDLING.equals(status))
            return "<span class=\"z-text-cyan\">处理中</span>";
        else if (HANGUP.equals(status))
            return "<span class=\"z-color-red\">已挂起</span>";
        else if (COMPLETED.equals(status))
            return "<span class=\"z-color-green\">已解决</span>";
        else if (CLOSED.equals(status))
            return "<span class=\"z-color-gray\">已关闭</span>";
        else if (REJECT.equals(status))
            return "<span class=\"z-color-blue\">已打回</span>";
        else
            return null;
    }
    
    /**
     * 获取问题类型中文描述
     * 
     * @param item  问题对象
     * @return      中文描述
     */
    public static String getIssueTypeDesc(GitIssue item)
    {
        switch (item.getIssueType())
        {
        case 1: return "<button class=\"z-button z-readonly z-blue\">需求</button>";
        case 2: return "<button class=\"z-button z-readonly z-purple\">BUG</button>";
        case 3: return "<button class=\"z-button z-readonly z-cyan\">缺陷</button>";
        default:return null;
        }
    }
    
    /**
     * 获取问题紧急性中文描述
     * 
     * @param item  问题对象
     * @return      中文描述
     */
    public static String getIssuePriorityDesc(GitIssue item)
    {
        switch (item.getIssuePriority())
        {
        case 1: return "<span class=\"z-color-orange\">!</span>";
        case 2: return "<span class=\"z-text-orange\">!!</span>";
        case 3: return "<span class=\"z-text-red\">!!!</span>";
        default:return null;
        }
    }
    
    /**
     * 获取BUG严重性中文描述
     * 
     * @param item  问题对象
     * @return      中文描述
     */
    public static String getBugLevelDesc(GitBugEx item)
    {
        switch (item.getBugLevel())
        {
        case 1: return "<span class=\"z-color-orange\">①</span>";
        case 2: return "<span class=\"z-text-orange\">②</span>";
        case 3: return "<span class=\"z-text-red\">③</span>";
        default:return null;
        }
    }
}
