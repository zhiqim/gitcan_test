/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;
import java.sql.Timestamp;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目功能表 对应表《GIT_FEATURE》
 */
@AnAlias("GitFeature")
@AnNew
@AnTable(table="GIT_FEATURE", key="FEATURE_ID", type="InnoDB")
public class GitFeature implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="MODULE_ID", type="long", notNull=true)    private long moduleId;    //2.模块编号
    @AnTableField(column="FEATURE_ID", type="long", notNull=true)    private long featureId;    //3.功能编号
    @AnTableField(column="FEATURE_NAME", type="string,32", notNull=true)    private String featureName;    //4.功能名称
    @AnTableField(column="FEATURE_DESC", type="string,128", notNull=true)    private String featureDesc;    //5.功能描述
    @AnTableField(column="FEATURE_STATUS", type="byte", notNull=true)    private int featureStatus;    //6.功能状态，0：正常，1：停用
    @AnTableField(column="FEATURE_SEQ", type="int", notNull=true)    private int featureSeq;    //7.功能排序数
    @AnTableField(column="FEATURE_CREATED", type="datetime", notNull=true)    private Timestamp featureCreated;    //8.创建时间
    @AnTableField(column="FEATURE_MODIFIED", type="datetime", notNull=true)    private Timestamp featureModified;    //9.更新时间

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public long getModuleId()
    {
        return moduleId;
    }

    public void setModuleId(long moduleId)
    {
        this.moduleId = moduleId;
    }

    public long getFeatureId()
    {
        return featureId;
    }

    public void setFeatureId(long featureId)
    {
        this.featureId = featureId;
    }

    public String getFeatureName()
    {
        return featureName;
    }

    public void setFeatureName(String featureName)
    {
        this.featureName = featureName;
    }

    public String getFeatureDesc()
    {
        return featureDesc;
    }

    public void setFeatureDesc(String featureDesc)
    {
        this.featureDesc = featureDesc;
    }

    public int getFeatureStatus()
    {
        return featureStatus;
    }

    public void setFeatureStatus(int featureStatus)
    {
        this.featureStatus = featureStatus;
    }

    public int getFeatureSeq()
    {
        return featureSeq;
    }

    public void setFeatureSeq(int featureSeq)
    {
        this.featureSeq = featureSeq;
    }

    public Timestamp getFeatureCreated()
    {
        return featureCreated;
    }

    public void setFeatureCreated(Timestamp featureCreated)
    {
        this.featureCreated = featureCreated;
    }

    public Timestamp getFeatureModified()
    {
        return featureModified;
    }

    public void setFeatureModified(Timestamp featureModified)
    {
        this.featureModified = featureModified;
    }

}
