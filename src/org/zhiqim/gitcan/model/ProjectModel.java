/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.model;

import org.zhiqim.gitcan.GitcanConstants;

/**
 * 项目计划总结模型
 *
 * @version v1.0.0 @author zouzhigang 2017-11-8 新建与整理
 */
public class ProjectModel implements GitcanConstants
{
    private long id;
    private int type;
    
    private int date;
    private int year;
    private int month;
    private int day;
    
    private String content;
    
    /***********************************************************/
    //设置标准参数
    /***********************************************************/
    
    public void setId(long id)
    {
        this.id = id;
    }

    public void setType(int type)
    {
        this.type = type;
    }
    
    public void setDate(int date)
    {
        this.date = date;
        
        this.year = date / 10000;
        this.month = date / 100 % 100;
        this.day = date % 10000;
    }
    
    public void setContent(String content)
    {
        this.content = content;
    }
    
    /***********************************************************/
    //获取标准参数
    /***********************************************************/
    
    public long getId()
    {
        return id;
    }
    
    public int getType()
    {
        return type;
    }
    
    public int getDate()
    {
        return date;
    }
    
    public String getContent()
    {
        return content;
    }
    
    /***********************************************************/
    //获取年月日明细
    /***********************************************************/

    public int getYear()
    {
        return year;
    }

    public int getMonth()
    {
        return month;
    }

    public int getDay()
    {
        return day;
    }
}
