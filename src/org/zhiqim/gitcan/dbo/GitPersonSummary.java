/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;
import java.sql.Timestamp;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 个人总结表 对应表《GIT_PERSON_SUMMARY》
 */
@AnAlias("GitPersonSummary")
@AnNew
@AnTable(table="GIT_PERSON_SUMMARY", key="SUMMARY_ID", type="InnoDB")
public class GitPersonSummary implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="OPERATOR_CODE", type="string,32", notNull=true)    private String operatorCode;    //1.操作员编号
    @AnTableField(column="SUMMARY_ID", type="long", notNull=true)    private long summaryId;    //2.总结编号
    @AnTableField(column="SUMMARY_TYPE", type="byte", notNull=true)    private int summaryType;    //3.总结类型，1：日总结，2：周总结，3：月总结
    @AnTableField(column="SUMMARY_DATE", type="int", notNull=true)    private int summaryDate;    //4.总结日期，格式yyyyMMdd，如20171101
    @AnTableField(column="SUMMARY_MODIFIED", type="datetime", notNull=true)    private Timestamp summaryModified;    //5.总结最后更新时间
    @AnTableField(column="SUMMARY_CONTENT", type="string,1000", notNull=false)    private String summaryContent;    //6.总结内容，最长1000字

    public String toString()
    {
        return Jsons.toString(this);
    }

    public String getOperatorCode()
    {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode)
    {
        this.operatorCode = operatorCode;
    }

    public long getSummaryId()
    {
        return summaryId;
    }

    public void setSummaryId(long summaryId)
    {
        this.summaryId = summaryId;
    }

    public int getSummaryType()
    {
        return summaryType;
    }

    public void setSummaryType(int summaryType)
    {
        this.summaryType = summaryType;
    }

    public int getSummaryDate()
    {
        return summaryDate;
    }

    public void setSummaryDate(int summaryDate)
    {
        this.summaryDate = summaryDate;
    }

    public Timestamp getSummaryModified()
    {
        return summaryModified;
    }

    public void setSummaryModified(Timestamp summaryModified)
    {
        this.summaryModified = summaryModified;
    }

    public String getSummaryContent()
    {
        return summaryContent;
    }

    public void setSummaryContent(String summaryContent)
    {
        this.summaryContent = summaryContent;
    }

}
