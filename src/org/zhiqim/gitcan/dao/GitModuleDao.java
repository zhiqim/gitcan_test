/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dao;

import java.util.List;

import org.zhiqim.gitcan.dbo.GitModule;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;

/**
 * 项目模块数据访问对象
 *
 * @version v1.0.0 @author zouzhigang 2017-11-9 新建与整理
 */
public class GitModuleDao
{
    /**
     * 获取当前请求的模块列表
     * 
     * @param request       请求
     * @return              模块列表
     * @throws Exception    异常
     */
    public static List<GitModule> list(HttpRequest request) throws Exception
    {
        Selector selector = new Selector();
        selector.addMust("projectId", GitProjectDao.getProjectId(request));
        selector.addOrderbyAsc("moduleSeq");
        
        return ORM.get(ZTable.class, request).list(GitModule.class, selector);
    }
    
    /**
     * 获取当前请求的模块列表
     * 
     * @param projectId     项目编号
     * @return              模块列表
     * @throws Exception    异常
     */
    public static List<GitModule> list(HttpRequest request, long projectId) throws Exception
    {
        Selector selector = new Selector();
        selector.addMust("projectId", projectId);
        selector.addOrderbyAsc("moduleSeq");
        
        return ORM.get(ZTable.class, request).list(GitModule.class, selector);
    }
}
