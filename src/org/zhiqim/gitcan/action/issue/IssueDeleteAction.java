/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.action.issue;

import org.zhiqim.gitcan.dbo.GitBug;
import org.zhiqim.gitcan.dbo.GitDefect;
import org.zhiqim.gitcan.dbo.GitIssue;
import org.zhiqim.gitcan.dbo.GitRequirement;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.ValidateAction;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;

/**
 * 项目问题删除
 *
 * @version v1.0.0 @author duanxiaohui 2017-10-12 新建与整理
 */
public class IssueDeleteAction extends ValidateAction
{
    @Override
    protected void validate(HttpRequest request)
    {
        request.addValidate(new IsNotEmpty("issueId", "请选择一个选项"));
    }

    @Override
    protected void perform(HttpRequest request) throws Exception
    {
        long issueId = request.getParameterLong("issueId");
        GitIssue issue = ORM.get(ZTable.class, request).item(GitIssue.class, issueId);
        if (issue == null)
        {
            request.returnHistory("该问题不存在，请重新选择");
            return;
        }
        
        ORM.get(ZTable.class, request).delete(GitIssue.class, issueId);
        switch (issue.getIssueType())
        {
        case 1:
            ORM.get(ZTable.class, request).delete(GitRequirement.class, issueId);
            GitMemberDao.report(request, request.getSessionName(), GitProjectDao.getProjectId(request), "", "删除了需求", issue.getIssueSeq());
            break;
        case 2:
            ORM.get(ZTable.class, request).delete(GitBug.class, issueId);
            GitMemberDao.report(request, request.getSessionName(), GitProjectDao.getProjectId(request), "", "删除了BUG", issue.getIssueSeq());
            break;
        case 3:
            ORM.get(ZTable.class, request).delete(GitDefect.class, issueId);
            GitMemberDao.report(request, request.getSessionName(), GitProjectDao.getProjectId(request), "", "删除了缺陷", issue.getIssueSeq());
            break;
        }
    }
}
