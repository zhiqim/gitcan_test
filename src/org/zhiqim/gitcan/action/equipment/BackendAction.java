/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.action.equipment;

import org.zhiqim.gitcan.dbo.GitBackend;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.StdSwitchAction;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.kernel.paging.PageResult;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.kernel.util.Validates;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;
import org.zhiqim.orm.dbo.Updater;


/**
 * 后台信息管理，支持后台信息的创建、删除和修改权限
 *
 * @version v1.0.0 @author duanxiaohui 2017-10-25 新建与整理
 */
public class BackendAction extends StdSwitchAction
{
    @Override
    protected void validateId(HttpRequest request)
    {
        request.addValidate(new IsNotEmpty("backendId", "请选择一个选项"));
    }

    @Override
    protected void validateForm(HttpRequest request)
    {
        request.addValidate(new IsNotEmpty("backendUrl", "后台地址不能为空"));
        request.addValidate(new IsNotEmpty("backendUser", "后台管理员不能为空"));
        request.addValidate(new IsNotEmpty("backendName", "后台名称不能为空"));
    }

    @Override
    protected void list(HttpRequest request) throws Exception
    {
        int pageNo = request.getParameterInt("page", 1);
        int pageSize = request.getContextAttributeInt("zmr_page_size", 20);
        
        Selector selector = new Selector("projectId", GitProjectDao.getProjectId(request));
        PageResult<GitBackend> result = ORM.get(ZTable.class, request).page(GitBackend.class, pageNo, pageSize, selector);
        result.addConditionMap(request.getParameterMap());
        
        request.setAttribute("result", result);
    }

    @Override
    protected void add(HttpRequest request) throws Exception
    {
    }

    @Override
    protected void modify(HttpRequest request) throws Exception
    {
        long backendId = request.getParameterLong("backendId");
        GitBackend item = ORM.get(ZTable.class, request).item(GitBackend.class, backendId);
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        request.setAttribute("item", item);
    }

    @Override
    protected void insert(HttpRequest request) throws Exception
    {
        String backendPassword = request.getParameter("backendPassword");
        if (Validates.isEmptyBlank(backendPassword))
        {
            request.returnHistory("后台管理员密码不能为空");
            return;
        }
        
        long projectId = GitProjectDao.getProjectId(request);
        String backendUrl = request.getParameter("backendUrl");
        if (ORM.get(ZTable.class, request).count(GitBackend.class, new Selector("projectId", projectId).addMust("backendUrl", backendUrl)) > 0)
        {
            request.returnHistory("该后台地址已存在，请不要重复增加");
            return;
        }
        
        String backendUser = request.getParameter("backendUser");
        String backendName = request.getParameter("backendName");
        
        GitBackend item = new GitBackend();
        item.setProjectId(projectId);
        item.setBackendId(Ids.longId());
        item.setBackendUser(backendUser);
        item.setBackendUrl(backendUrl);
        item.setBackendName(backendName);
        item.setBackendPassword(backendPassword);
        
        ORM.get(ZTable.class, request).insert(item);
        
        GitMemberDao.report(request, projectId, request.getSessionName(), "创建了管理台", backendName);
    }

    @Override
    protected void update(HttpRequest request) throws Exception
    {
        long backendId = request.getParameterLong("backendId");
        GitBackend item = ORM.get(ZTable.class, request).item(GitBackend.class, backendId);
        if(item == null)
        {
            request.returnHistory("后台不存在，请重新选择");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        Updater updater = new Updater();
        updater.addMust("backendId", backendId);
        updater.addField("backendUser", request.getParameter("backendUser"));
        updater.addField("backendUrl", request.getParameter("backendUrl"));
        updater.addField("backendName", request.getParameter("backendName"));
        
        String backendPassword = request.getParameter("backendPassword");
        if (Validates.isNotEmpty(backendPassword))
        {//密码不为空才修改
            updater.addField("backendPassword", backendPassword);
        }
        
        ORM.get(ZTable.class, request).update(GitBackend.class, updater);
        
        GitMemberDao.report(request, item.getProjectId(), request.getSessionName(), "修改了管理台", request.getParameter("backendName"));
    }

    @Override
    protected void delete(HttpRequest request) throws Exception
    {
        long backendId = request.getParameterLong("backendId");
        GitBackend item = ORM.get(ZTable.class, request).item(GitBackend.class, backendId);
        if(item == null)
        {
            request.returnHistory("后台不存在，请重新选择");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        ORM.get(ZTable.class, request).delete(GitBackend.class, backendId);
        
        GitMemberDao.report(request, item.getProjectId(), request.getSessionName(), "删除了管理台", item.getBackendName());
    }
}
