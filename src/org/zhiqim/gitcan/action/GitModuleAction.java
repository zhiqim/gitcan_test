/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * gitcan is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.action;

import java.sql.Timestamp;
import java.util.List;

import org.zhiqim.gitcan.dbo.GitFeature;
import org.zhiqim.gitcan.dbo.GitModule;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.StdSwitchAction;
import org.zhiqim.httpd.validate.ones.IsIntegerValue;
import org.zhiqim.httpd.validate.ones.IsLen;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.kernel.util.Sqls;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;
import org.zhiqim.orm.dbo.Updater;

/**
 * 项目模块
 *
 * @version v1.0.0 @author huangsufang 2017-10-11 新建与整理
 */
public class GitModuleAction extends StdSwitchAction
{
    @Override
    protected void validateId(HttpRequest request)
    {
        request.addValidate(new IsNotEmpty("moduleId", "请选择一个选项"));
    }

    @Override
    protected void validateForm(HttpRequest request)
    {
        request.addValidate(new IsLen("moduleName", "模块名称不能为空且不超过32个字符", 1, 32));
        request.addValidate(new IsIntegerValue("moduleSeq", "模块排序必须是[0, 999999]范围的非负整数", 0, 999999));
    }

    @Override
    protected void list(HttpRequest request) throws Exception
    {
        Selector selector = new Selector();
        selector.addMust("projectId", GitProjectDao.getProjectId(request));
        selector.addOrderbyAsc("moduleSeq");
        
        List<GitModule> list = ORM.get(ZTable.class, request).list(GitModule.class, selector);
        request.setAttribute("list", list);
    }

    @Override
    protected void add(HttpRequest request) throws Exception
    {
    }

    @Override
    protected void modify(HttpRequest request) throws Exception
    {
        long moduleId = request.getParameterLong("moduleId");
        GitModule item = ORM.get(ZTable.class, request).item(GitModule.class, moduleId);
        if (item == null)
        {
            request.returnHistory("模块不存在，请重新选择！");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
             
        request.setAttribute("item", item);
    }

    @Override
    protected void insert(HttpRequest request) throws Exception
    {
        long projectId = GitProjectDao.getProjectId(request);
        Timestamp dateTime = Sqls.nowTimestamp();

        GitModule module = request.getParameter(GitModule.class);
        module.setProjectId(projectId);
        module.setModuleId(Ids.longId());
        module.setModuleCreated(dateTime);
        module.setModuleModified(dateTime);

        ORM.get(ZTable.class, request).insert(module);
        
        GitMemberDao.report(request, projectId, request.getSessionName(), "创建了模块", module.getModuleName());
    }

    @Override
    protected void update(HttpRequest request) throws Exception
    {
        long moduleId = request.getParameterLong("moduleId");
        GitModule item = ORM.get(ZTable.class, request).item(GitModule.class, moduleId);
        if(item == null)
        {
            request.returnHistory("模块不存在，请重新选择");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        Updater updater = new Updater();
        updater.addMust("moduleId", moduleId);
        updater.addField("moduleModified", Sqls.nowTimestamp());
        updater.addField("moduleSeq", request.getParameterInt("moduleSeq"));
        updater.addField("moduleName", request.getParameter("moduleName"));
        updater.addField("moduleDesc", request.getParameter("moduleDesc"));
        
        ORM.get(ZTable.class, request).update(GitModule.class, updater);
        
        GitMemberDao.report(request,item.getProjectId(), request.getSessionName(), "修改了模块", request.getParameter("moduleName"));
    }

    @Override
    protected void delete(HttpRequest request) throws Exception
    {
        long moduleId = request.getParameterLong("moduleId");
        GitModule item = ORM.get(ZTable.class, request).item(GitModule.class, moduleId);
        if(item == null)
        {
            request.returnHistory("模块不存在，请重新选择");
            return;
        }
        
        if (!GitProjectDao.chkProjectId(request, item.getProjectId()))
        {//判断是否切换了默认项目
            return;
        }
        
        if (ORM.get(ZTable.class, request).count(GitFeature.class, new Selector("moduleId", moduleId)) > 0)
        {
            request.returnHistory("该模块下存在功能，请先删除功能！");
            return;
        }
        
        ORM.get(ZTable.class, request).delete(GitModule.class, moduleId);
        
        GitMemberDao.report(request, item.getProjectId(), request.getSessionName(), "删除了模块", item.getModuleName());
    }

}
