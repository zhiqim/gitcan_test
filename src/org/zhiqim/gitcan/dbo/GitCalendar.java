/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitcan.htm
 *
 * Zhiqim Console is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitcan.dbo;

import java.io.Serializable;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 工作日历表 对应表《GIT_CALENDAR》
 */
@AnAlias("GitCalendar")
@AnNew
@AnTable(table="GIT_CALENDAR", key="CALENDAR_DATE", type="InnoDB")
public class GitCalendar implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="CALENDAR_DATE", type="int", notNull=true)    private int calendarDate;    //1.日历日期，8位数字，格式：20170101
    @AnTableField(column="CALENDAR_REST", type="boolean", notNull=true)    private boolean calendarRest;    //2.是否休息日，1：表示休息日，0：表示工作日
    @AnTableField(column="CALENDAR_DESC", type="string,100", notNull=false)    private String calendarDesc;    //3.日期说明，如休息日说明什么节假日，工作日说明调整原因

    public String toString()
    {
        return Jsons.toString(this);
    }

    public int getCalendarDate()
    {
        return calendarDate;
    }

    public void setCalendarDate(int calendarDate)
    {
        this.calendarDate = calendarDate;
    }

    public boolean isCalendarRest()
    {
        return calendarRest;
    }

    public void setCalendarRest(boolean calendarRest)
    {
        this.calendarRest = calendarRest;
    }

    public String getCalendarDesc()
    {
        return calendarDesc;
    }

    public void setCalendarDesc(String calendarDesc)
    {
        this.calendarDesc = calendarDesc;
    }

}
